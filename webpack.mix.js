const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// removido para instalar foundation 
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .minify('public/css/app.css')
    .copy('node_modules/foundation-sites/dist/js/foundation.min.js', 'public/js')
    .copy('node_modules/jquery/dist/jquery.min.js', 'public/js')
    .options({ purifyCss: true });

mix.options({
    cleanCss: {
        level: {
            1: {
                specialComments: 'none'
            }
        }
    }
});
/*
   */ 
// mix.sass('resources/assets/sass/app.scss', 'public/css');
// mix.combine(['resources/assets/js/app.js',
// //     'node_modules/jquery/dist/jquery.min.js',
//     'node_modules/foundation-sites/dist/js/foundation.min.js'
// ], 'public/js/app.js');

