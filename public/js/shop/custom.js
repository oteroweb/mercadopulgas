jQuery(document).ready(function() {
    "use strict";
    $(window).on('load',function() {
        $('.body-wrapper').each(function() {
            var header_area = $('.header');
            var main_area = header_area.children('.navbar');
            var logo = main_area.find('.navbar-header');
            var navigation = main_area.find('.navbar-collapse');
            var original = { nav_top: navigation.css('margin-top')  };
            $(window).scroll(function() {
                if (main_area.hasClass('bb-fixed-header') && ($(this).scrollTop() == 0 || $(this).width() < 750)) {
                    main_area.removeClass('bb-fixed-header').appendTo(header_area);
                    navigation.animate({
                        'margin-top': original.nav_top
                    }, {
                        duration: 100,
                        queue: false,
                        complete: function() {
                            header_area.css('height', 'auto');
                        }
                    });
                } else if (!main_area.hasClass('bb-fixed-header') && $(this).width() > 750 && $(this).scrollTop() > header_area.offset().top + header_area.height() - parseInt($('html').css('margin-top'))) {
                    header_area.css('height', header_area.height());
                    main_area.css({
                        'opacity': '0'
                    }).addClass('bb-fixed-header');
                    main_area.appendTo($('body')).animate({
                        'opacity': 1
                    });
                    navigation.css({
                        'margin-top': '0px'
                    });
                }
            });
        });
        $(window).trigger('resize'); 
        $(window).trigger('scroll');
    });
});
if ($('.navbar').width() > 1007) {
    $('.nav .dropdown').on('mouseover', function() {
        $(this).addClass('open');
    }), $('.nav .dropdown').on('mouseleave', function() {
        $(this).removeClass('open');
    });
}
$('.nav-category .dropdown-submenu ').hover(function() {
    $(this).addClass('open');
}, function() {
    $(this).removeClass('open');
});
jQuery(document).ready(function() {
    $('.searchBox a').on("click", function() {
        $(".searchBox .dropdown-menu").toggleClass('display-block');
        $(".searchBox a i").toggleClass('fa-close').toggleClass("fa-search");
    });
});
jQuery(document).ready(function() {
    "use strict";
    var owl = $('.owl-carousel.featuredProductsSlider');
    owl.owlCarousel({
        loop: true,
        margin: 28,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        nav: true,
        moveSlides: 4,
        dots: false,
        responsive: {
            320: { items: 1 }, 768: { items: 3 }, 992: { items: 4 }
        }
    });
});

jQuery(document).ready(function() {
    $('.select-drop').selectbox();
});
jQuery(document).ready(function() {
    $('.side-nav li a').click(function() {
        $(this).find('i').toggleClass('fa fa-minus fa fa-plus');
    });
});

jQuery(document).ready(function() {
    $(".quick-view .btn-block").click(function() {
        $(".quick-view").modal("hide");
    });
});