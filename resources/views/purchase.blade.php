@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Comprando un articulo</div>
                <div class="panel-body">

                                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if (session('msg')) <div class="alert alert-success"> {{ session('msg') }} </div>
                    @endif
    <form class="form-horizontal" role="form" method="POST" action="{{ route('validate.purchase',['slug' => $product->slug ]) }}">
        {{ csrf_field() }}

                        Advertendia este paso no puede ser desecho, y puede generar cargos
                        <br> Esta realizando la compra por el articulo
                        <br> 
                        Nombre del articulo = {{$product->name}}
                        <br> 
                        Costo Total en tu compra = {{$product->price_sell*$quantity}} Bs.F
                        <br>
                        Foto del Articulo = <img src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> 
                        <br>
                        Cantidad = {{$quantity}}        
                        <br>

                        Ingrese su contraseña nuevamente para continuar 
        
                        <!-- Select: cantidad de producto -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Cantidad </label>

                            <div class="col-md-6">
                                 <input id="password" type="password" required class="form-control" name="password" value="{{ old('password') }}" >
                                @if ($errors->has('password'))
                                    <span   class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: cantidad de producto> -->

    <button type="submit" class="btn btn-success"> comprar </button>
                        
                </form>            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
