  @extends('layouts.app')

  @section('content')

  <div class="container">
          
                      @if( is_null($shop) )
                  <h1>Categoria <a href="{{route('category',['category' =>$current_category->slug])}} ">{{$current_category->name}} </a></h1> 
                        @else
                        <link class="estilo" href="{{asset('storage/themes/')}}/{{(Auth::user()->template != '') ? Auth::user()->template : ''}}.css" rel="stylesheet">

                  <h1>Categoria <a href="{{route('shop.category',['category' =>$current_category->slug,'shop'=> $shop])}} ">{{$current_category->name}} </a></h1> 
                       @endif   
          <div class="product">

              <div class="container-fliud">
                  <div class="wrapper row">
                      <div class="preview col-md-6">
                          
                          <div class="preview-pic tab-content">
                            <div class="tab-pane active" id="pic-1"><img src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> </div>
                          </div>
                          <ul class="preview-thumbnail nav nav-tabs">
                            <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> </a></li>
                          </ul>
                          
                      </div>
                      <div class="details col-md-6">

                  <h1></h1>

                          <h3 class="product-title">{{$product->promotional_title}}</h3>
                          <h3 class="product-subtitle">{{$product->promotional_subtitle}}</h3>
                          <p class="product-description">{{$product->description }}
                          </p>
                          <h4 class="price">Precio:<span>{{$product->price_sell}}Bs.F.</span></h4>
                          <div class="action">
                              @include('purchaseForm')
                          </div>
                      </div>
                  </div>
              </div>
          </div>

                   @include('questions')
      </div>

  <div class="container" style="display:none;" >
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-primary">
                  <div class="panel-heading">Dashboard</div>
                       @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                      @endif
                  <div class="panel-body">
                      <!-- <p class="categories__title-list">Categorias</p> -->
                      <ul class="categories-list">
                      @if( is_null($shop) )
                          @foreach ($categories as $category)
                              <li>
                                  <a href="{{route('category',['category' => $category->slug]) }} ">{{ $category->name }}</a>
                              </li>
                          @endforeach
                      </ul>
                          @else
                      <ul class="categories-list">
                          @foreach ($categories as $category)
                              <li>
                                  <a href="{{route('shop.category',['category' => $category->slug,'shop'=>$shop]) }} ">{{ $category->name }}</a>
                              </li>
                          @endforeach
                      </ul>
                      @endif   

              
                  <h1>Producto</h1>
                   <img src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> 

                  </div>
              </div>
          </div>
      </div>
  </div>
  @endsection


  <style type="text/css">
      
  /*****************globals*************/
  img {
    max-width: 100%; }

  .preview {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
        -ms-flex-direction: column;
            flex-dminilyricsirection: column; }
    @media screen and (max-width: 996px) {
      .preview {
        margin-bottom: 20px; } }

  .preview-pic {
    -webkit-box-flex: 1;
    -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
            flex-grow: 1; }

  .preview-thumbnail.nav-tabs {
    border: none;
    margin-top: 15px; }
    .preview-thumbnail.nav-tabs li {
      width: 18%;
      margin-right: 2.5%; }
      .preview-thumbnail.nav-tabs li img {
        max-width: 100%;
        display: block; }
      .preview-thumbnail.nav-tabs li a {
        padding: 0;
        margin: 0; }
      .preview-thumbnail.nav-tabs li:last-of-type {
        margin-right: 0; }

  .tab-content {
    overflow: hidden; }
    .tab-content img {
      width: 100%;
      -webkit-animation-name: opacity;
              animation-name: opacity;
      -webkit-animation-duration: .3s;
              animation-duration: .3s; }

  .product {
    margin-top: 50px;
    background: #eee;
    padding: 3em;
    line-height: 1.5em; }

  @media screen and (min-width: 997px) {
    .wrapper {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex; } }

  .details {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
        -ms-flex-direction: column;
            flex-direction: column; }

  .colors {
    -webkit-box-flex: 1;
    -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
            flex-grow: 1; }

  .product-title, .price, .sizes, .colors {
    text-transform: UPPERCASE;
    font-weight: bold; }

  .checked, .price span {
    color: #ff9f1a; }

  .product-title, .rating, .product-description, .price, .vote, .sizes {
    margin-bottom: 15px; }

  .product-title {
    margin-top: 0; }

  .size {
    margin-right: 10px; }
    .size:first-of-type {
      margin-left: 40px; }

  .color {
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
    height: 2em;
    width: 2em;
    border-radius: 2px; }
    .color:first-of-type {
      margin-left: 20px; }

  .add-to-cart, .like {
    background: #ff9f1a;
    padding: 1.2em 1.5em;
    border: none;
    text-transform: UPPERCASE;
    font-weight: bold;
    color: #fff;
    -webkit-transition: background .3s ease;
            transition: background .3s ease; }
    .add-to-cart:hover, .like:hover {
      background: #b36800;
      color: #fff; }

  .not-available {
    text-align: center;
    line-height: 2em; }
    .not-available:before {
      font-family: fontawesome;
      content: "\f00d";
      color: #fff; }


  @-webkit-keyframes opacity {
    0% {
      opacity: 0;
      -webkit-transform: scale(3);
              transform: scale(3); }
    100% {
      opacity: 1;
      -webkit-transform: scale(1);
              transform: scale(1); } }

  @keyframes opacity {
    0% {
      opacity: 0;
      -webkit-transform: scale(3);
              transform: scale(3); }
    100% {
      opacity: 1;
      -webkit-transform: scale(1);
              transform: scale(1); } }

  /*# sourceMappingURL=style.css.map */

  .product-description{
          background: rgba(255, 255, 255, 0.7);
      font-weight: bolder;
      margin: 3em 1em;
      padding: 2em;
  }

     .no-question {
              margin: 2em;
      text-align: center;
      background: rgba(172, 163, 181, 0.68);
      padding: 2em;
      border-radius: 1em;
      }
      .question-input{

      border-radius: 3em;

      }
      .question{
          margin:2em;
      }
      .answer{
          margin-left: 4em;
      }


  </style>