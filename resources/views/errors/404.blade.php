@extends('layouts.app')

@section('content')
<div class="col-md-10 col-md-offset-1">
<div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600; margin-top: 10vh;">
<a href="{{route("welcome")}}" style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Este podria ser el enlace de tu tienda registrate y ten tu tienda virtual en minutos!</a><a class="btn btn-default btn-sm" href="{{route("register")}}" style="margin-top: -5px; border: 0px; box-shadow: none; color: rgb(243, 156, 18); font-weight: 600; background: rgb(255, 255, 255);">Registrarme</a></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pagina no encontrada
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route("welcome")}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Pagina no encontrada</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="col-md-10 col-md-offset-1">
      <div class="error-page">
        {{-- <h2 class="headline text-yellow"> 404</h2> --}}

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! La pagina que estas buscando no existe.</h3>

          <p>
            La pagina que intentas acceder no existe, por favor <a href="{{route("welcome")}}">regresar al inicio</a> o busca el producto que buscas.
          </p>

          <form class="search-form" action="{{route("search.products")}}" method="GET">
            <div class="input-group">
              <input type="text" name="search" class="form-control" placeholder="Buscar Productos">

              <div class="input-group-btn">
                <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search">Busqueda</i>
                </button>
              </div>
            </div>
            <!-- /.input-group -->
          </form>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    <div>
</div></section>
    <!-- /.content -->
  </div>
@endsection

