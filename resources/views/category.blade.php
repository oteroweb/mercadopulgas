@extends('layouts.app')

@section('content')
<div class="container">

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading text-center"><h3>Productos por categoria</h3></div>
        <div class="panel-body">
          <div class="col-md-3">
            @include('category-view')
          </div>
          <div class="col-md-9 product-list">
            @include('product-view')
          </div>
        </div>
    </div>
  </div>
</div>

</div>
@endsection
<style>
.categories-list{display: flex;
    flex-direction: column;}
.product-list{ border-left: 3px solid grey; }
.product-list-group{ height: 50vh; }
.product-list-group:hover{ border-radius:20px; border:1px solid rgba(0,0,0,.3);}
.product-list-group>a:hover{text-decoration: none;}
.product-list-title{  }
.product-list-subtitle{  }
.product-list-img{  }
.product-list-price{  }
</style>
