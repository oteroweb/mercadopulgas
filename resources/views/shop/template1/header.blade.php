  <!-- HEADER -->
      <div class="header clearfix">
        <!-- TOPBAR -->
        <div class="topBar">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-sm-5 hidden-xs">
              @if($options->social_network != "")
                <ul class="list-inline">
                @foreach ($options->social_network as $key => $value)
                  <li><a href="{{$value}}"><i class="fa fa-{{$key}}"></i></a></li>
                @endforeach
                </ul>
                  @endif
              </div>
              <div class="col-md-6 col-sm-7 col-xs-12">
                <ul class="list-inline pull-right top-right">
                  <li class="account-login"><span><a data-toggle="modal" href='#login'>Iniciar Sesion</a><small>or</small><a data-toggle="modal" href='#signup'>Crear una cuenta</a></span></li>
                  <li class="searchBox">
                    <a href="#"><i class="fa fa-search"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li>
                        <span class="input-group">
                          <input type="text" class="form-control" placeholder="Search…" aria-describedby="basic-addon2">
                          <button type="submit" class="input-group-addon">Submit</button>
                        </span>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown cart-dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i>$0</a>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li>Articulo(s) en tu Carrito</li>
                      <li>
                        <a href="single-product.html">
                          <div class="media">
                            <img class="media-left media-object" src="{{asset('images/shop/home/cart-items/cart-item-01.jpg')}}" alt="cart-Image">
                            <div class="media-body">
                              <h5 class="media-heading">INCIDIDUNT UT <br><span>2 X $199</span></h5>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="single-product.html">
                          <div class="media">
                            <img class="media-left media-object" src="{{asset('images/shop/home/cart-items/cart-item-01.jpg')}}" alt="cart-Image">
                            <div class="media-body">
                              <h5 class="media-heading">INCIDIDUNT UT <br><span>2 X $199</span></h5>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <div class="btn-group" role="group" aria-label="...">
                          <button type="button" class="btn btn-default" onclick="location.href='cart-page.html';">Shopping Cart</button>
                          <button type="button" class="btn btn-default" onclick="location.href='checkout-step-1.html';">Checkout</button>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- NAVBAR -->
        <nav class="navbar navbar-main navbar-default" role="navigation">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.html">
                @if ($options->logo_image != "") 
                <img src="{{asset("images/logo.png")}}" alt="logo">" 
                @else 
                <h1 class="brand">{{$options->sitename}}</h1>
                
                @endif
              </a>

                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                  <ul class="dropdown-menu dropdown-menu-left">
                    <li><a href="index.html">Home Default</a></li>
                  </ul>
                </li>
                <li class="dropdown megaDropMenu">
                  <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="300" data-close-others="true" aria-expanded="false">Shop</b></a>
                  <ul class="dropdown-menu row">
                    <li class="col-sm-3 col-xs-12">
                      <ul class="list-unstyled">
                        <li>Products Grid View</li>
                        <li><a href="product-grid-left-sidebar.html">Products Sidebar Left</a></li>
                      </ul>
                    </li>
                    <li class="col-sm-3 col-xs-12">
                      <ul class="list-unstyled">
                        <li>Products List View</li>
                        <li><a href="product-list-left-sidebar.html">Products Sidebar Left</a></li>
                        <li><a href="product-list-right-sidebar.html">Products Sidebar Right</a></li>
                        <li class="listHeading">Others</li>
                        <li><a href="single-product.html">Single Product</a></li>
                        <li><a href="cart-page.html">Cart Page</a></li>
                      </ul>
                    </li>
                    <li class="col-sm-3 col-xs-12">
                      <ul class="list-unstyled">
                        <li>Checkout</li>
                        <li><a href="checkout-step-1.html">Step 1 - Address</a></li>
                      </ul>
                    </li>
                    <li class="col-sm-3 col-xs-12">
                      <a href="#" class="menu-photo"><img src="{{asset('images/shop/menu-photo.jpg')}}" alt="menu-img"></a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div>
        </nav>
      </div>