@extends('layouts.shop') 
@section('content')
@include('shop.template1.header')
      <!-- BANNER --> 
      {{-- 
      <div class="bannercontainer bannerV1">
        <div class="fullscreenbanner-container">
          <div class="fullscreenbanner">
            <ul>
              <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                <img src="{{asset('images/shop/home/banner-slider/slider-bg.jpg')}}" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="slider-caption slider-captionV1 container">

                  <div 
                  class="tp-caption rs-caption-1 sft start"
                    data-start="1500"
                    data-hoffset="0" 
                    data-easing="Back.easeInOut"
                    data-x="370"
                    data-y="54"
                    data-speed="800"
                    data-endspeed="300"
                     >
                    <img src="{{asset('images/shop/home/banner-slider/shoe1.png')}}" alt="slider-image" style="width: 781px; height: 416px;">
                  </div>

                  <div class="tp-caption rs-caption-2 sft"
                    data-hoffset="0"
                    data-y="119"
                    data-speed="800"
                    data-start="2000"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    Canvas Sneaker
                  </div>

                  <div class="tp-caption rs-caption-3 sft"
                    data-hoffset="0"
                    data-y="185"
                    data-speed="1000"
                    data-start="3000"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">
                    Exclusive to <br>
                    BigBag <br>
                    <small>Spring / Summer 2016</small>
                  </div>
                  <div class="tp-caption rs-caption-4 sft"
                    data-hoffset="0"
                    data-y="320"
                    data-speed="800"
                    data-start="3500"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">
                    <span class="page-scroll"><a href="#" class="btn primary-btn">Buy Now<i class="glyphicon glyphicon-chevron-right"></i></a></span>
                  </div>
                </div>

              </li>
               <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="1000" data-title="Slide 2">
                <img src="{{asset('images/shop/home/banner-slider/slider-bg.jpg')}}" alt="slidebg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="slider-caption slider-captionV1 container captionCenter">
                  <div class="tp-caption rs-caption-1 sft start text-center"
                    data-x="center"
                    data-y="228"
                    data-speed="800"
                    data-start="1500"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    <img src="{{asset('images/shop/home/banner-slider/shoe2.png')}}" alt="slider-image">
                  </div>

                  <div class="tp-caption rs-caption-2 sft text-center"
                    data-x="center"
                    data-y="50"
                    data-speed="800"
                    data-start="2000"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    Exclusive to BigBag
                  </div>

                  <div class="tp-caption rs-caption-3 sft text-center"
                    data-x="center"
                    data-y="98"
                    data-speed="1000"
                    data-start="3000"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">
                    Canvas Sneaker
                  </div>

                  <div class="tp-caption rs-caption-4 sft text-center"
                    data-x="center"
                    data-y="156"
                    data-speed="800"
                    data-start="3500"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">
                    <span class="page-scroll"><a href="#" class="btn primary-btn">Buy Now<i class="glyphicon glyphicon-chevron-right"></i></a></span>
                  </div>
                </div>
              </li> 
              
              <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide 3">
                <img src="{{asset('images/shop/home/banner-slider/slider-bg.jpg')}}" alt="slidebg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="slider-caption slider-captionV1 container">
                  <div class="tp-caption rs-caption-1 sft start"
                    data-hoffset="0"
                    data-y="85"
                    data-speed="800"
                    data-start="1500"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    <img src="{{asset('images/shop/home/banner-slider/shoe3.png')}}" alt="slider-image">
                  </div>

                  <div class="tp-caption rs-caption-2 sft "
                    data-hoffset="0"
                    data-y="119"
                    data-x="800"
                    data-speed="800"
                    data-start="2000"
                    data-easing="Back.easeInOut"
                    data-endspeed="300">
                    Canvas Sneaker
                  </div>

                  <div class="tp-caption rs-caption-3 sft"
                    data-hoffset="0"
                    data-y="185"
                    data-x="800"
                    data-speed="1000"
                    data-start="3000"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">
                    Exclusive to <br>
                    BigBag <br>
                    <small>Spring / Summer 2016</small>
                  </div>

                  <div class="tp-caption rs-caption-4 sft"
                    data-hoffset="0"
                    data-y="320"
                    data-x="800"
                    data-speed="800"
                    data-start="3500"
                    data-easing="Power4.easeOut"
                    data-endspeed="300"
                    data-endeasing="Power1.easeIn"
                    data-captionhidden="off">
                    <span class="page-scroll"><a href="#" class="btn primary-btn">Buy Now<i class="glyphicon glyphicon-chevron-right"></i></a></span>
                  </div>
                </div>
              </li> 
              
            </ul>
          </div>
        </div>
      </div>       --}}

      <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    @foreach ($options->header as $key => $node)
        <li data-target="#myCarousel" class="{{($key == 0) ? 'active' : ''}}" data-slide-to="{{ $key }}"></li>
    @endforeach

  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    @foreach ($options->header as $key => $value)
        <div class="item {{($key == 0) ? 'active' : ''}}">
            <img src="{{asset($value->image)}}" alt="Los Angeles">
        </div>
    @endforeach
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix">
        <div class="container">
          <div class="row featuredCollection margin-bottom">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Nuestros Productos Recientes</h4>
              </div>
            </div>
            <div class="col-sm-4 col-xs-12">
              <div class="thumbnail" onclick="location.href='single-product.html';">
                <div class="imageWrapper">
                  <img src="{{asset('images/shop/home/featured-collection/featured-collection-01.jpg')}}" alt="feature-collection-image">
                  <div class="masking"><a href="product-grid-left-sidebar.html" class="btn viewBtn">View Prodocts</a></div>
                </div>
                <div class="caption">
                  <h4>Shoes</h4>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-xs-12">
              <div class="thumbnail" onclick="location.href='single-product.html';">
                <div class="imageWrapper">
                  <img src="{{asset('images/shop/home/featured-collection/featured-collection-02.jpg')}}" alt="feature-collection-image">
                  <div class="masking"><a href="product-grid-left-sidebar.html" class="btn viewBtn">View Prodocts</a></div>
                </div>
                <div class="caption">
                  <h4>Bags</h4>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-xs-12">
              <div class="thumbnail" onclick="location.href='single-product.html';">
                <div class="imageWrapper">
                  <img src="{{asset('images/shop/home/featured-collection/featured-collection-03.jpg')}}" alt="feature-collection-image">
                  <div class="masking"><a href="product-grid-left-sidebar.html" class="btn viewBtn">View Prodocts</a></div>
                </div>
                <div class="caption">
                  <h4>Glasses</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="row featuredProducts margin-bottom">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Los mas Vendidos</h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">
                <div class="slide">
                  <div class="productImage clearfix">
                    <img src="{{asset('images/shop/home/featured-product/product-01.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href="login" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" href="#quick" class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption clearfix">
                    <a href="single-product.html">
                      <h5>Mauris efficitur</h5>
                    </a>
                    <h3>$199</h3>
                  </div>
                </div>
                <div class="slide">
                  <div class="productImage">
                    <img src="{{asset('images/shop/home/featured-product/product-02.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href="#login" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" href="#quick" class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption">
                    <a href="single-product.html">
                      <h5>Nulla facilisi</h5>
                    </a>
                    <h3>$149</h3>
                  </div>
                </div>
                <div class="slide">
                  <div class="productImage">
                    <img src="{{asset('images/shop/home/featured-product/product-03.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href="#login" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" href="#quick" class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption">
                    <a href="single-product.html">
                      <h5>Praesent dui felis</h5>
                    </a>
                    <h3>$79</h3>
                  </div>
                </div>
                <div class="slide">
                  <div class="productImage">
                    <img src="{{asset('images/shop/home/featured-product/product-04.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href=".html" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" quick class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption">
                    <a href="single-product.html">
                      <h5>Cras eu faucibus</h5>
                    </a>
                    <h3>$109</h3>
                  </div>
                </div>
                <div class="slide">
                  <div class="productImage">
                    <img src="{{asset('images/shop/home/featured-product/product-05.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href=".html" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" href="#quick"  class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption">
                    <a href="single-product.html">
                      <h5>Mauris lobortis augue</h5>
                    </a>
                    <h3>$199</h3>
                  </div>
                </div>
                <div class="slide">
                  <div class="productImage">
                    <img src="{{asset('images/shop/home/featured-product/product-06.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href=".html" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" href="#quick"  class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption">
                    <a href="single-product.html">
                      <h5>Suspendisse suscipit</h5>
                    </a>
                   <h3>$149</h3>
                  </div>
                </div>
                <div class="slide">
                  <div class="productImage">
                    <img src="{{asset('images/shop/home/featured-product/product-07.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href=".html" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" href="#quick"  class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption">
                    <a href="single-product.html">
                      <h5>Praesent a augue</h5>
                    </a>
                    <h3>$79</h3>
                  </div>
                </div>
                <div class="slide">
                  <div class="productImage">
                    <img src="{{asset('images/shop/home/featured-product/product-09.jpg')}}" alt="featured-product-img">
                    <div class="productMasking">
                      <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href="#login" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li><a href="cart-page.html" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a data-toggle="modal" href="#quick" class="btn btn-default"><i class="fa fa-eye"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="productCaption">
                    <a href="single-product.html">
                      <h5>Cras vel augue</h5>
                    </a>
                    <h3>$109</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row latestArticles">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Novedades</h4>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="thumbnail">
                <h5><a href="blog-single-right-sidebar.html">Donec egestas tortor quis mattis</a></h5>
                <span class="meta">July 16, 2016 by <a href="#">Abdus</a></span>
                <a href="blog-single-right-sidebar.html"><img src="{{asset('images/shop/home/articles/articles-01.jpg')}}" alt="article-image"></a>
                <div class="caption">
                  <p>Nulla facilisi. Mauris efficitur, massa et iaculis accumsan, mauris velit ultrices purus, quis condimentum nibh dolor ut tortor.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="thumbnail">
                <h5><a href="blog-single-right-sidebar.html">Pellentesque risus quis tellus</a></h5>
                <span class="meta">July 16, 2016 by <a href="#">Abdus</a></span>
                <a href="blog-single-right-sidebar.html"><img src="{{asset('images/shop/home/articles/articles-02.jpg')}}" alt="article-image"></a>
                <div class="caption">
                  <p>Praesent dui felis, gravida a auctor at, facilisis commodo ipsum. Cras eu faucibus justo. Nullam varius cursus nisi.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="thumbnail">
                <h5><a href="blog-single-right-sidebar.html">Mauris lobortis augue ex</a></h5>
                <span class="meta">July 16, 2016 by <a href="#">Abdus</a></span>
                <a href="blog-single-right-sidebar.html"><img src="{{asset('images/shop/home/articles/articles-03.jpg')}}" alt="article-image"></a>
                <div class="caption">
                  <p>Etiam aliquam turpis quis blandit finibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="thumbnail">
                <h5><a href="blog-single-right-sidebar.html">Suspendisse vestibulum dignissim</a></h5>
                <span class="meta">July 16, 2016 by <a href="#">Abdus</a></span>
                <a href="blog-single-right-sidebar.html"><img src="{{asset('images/shop/home/articles/articles-04.jpg')}}" alt="article-image"></a>
                <div class="caption">
                  <p>Suspendisse tristique interdum est, at hendrerit nunc laoreet et. Nulla vel arcu ac turpis pulvinar tincidunt eu eu nisi.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- LIGHT SECTION -->
      <section class="lightSection clearfix">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="owl-carousel partnersLogoSlider">
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-01.png')}}" alt="partner-img">
                  </div>
                </div>
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-02.png')}}" alt="partner-img">
                  </div>
                </div>
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-03.png')}}" alt="partner-img">
                  </div>
                </div>
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-04.png')}}" alt="partner-img">
                  </div>
                </div>
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-05.png')}}" alt="partner-img">
                  </div>
                </div>
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-01.png')}}" alt="partner-img">
                  </div>
                </div>
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-02.png')}}" alt="partner-img">
                  </div>
                </div>
                <div class="slide">
                  <div class="partnersLogo clearfix">
                    <img src="{{asset('images/shop/home/partners/partner-03.png')}}" alt="partner-img">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection
