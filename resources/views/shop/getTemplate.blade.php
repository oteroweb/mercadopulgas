@extends('layouts.app') 

@section('content')
<link href="{{asset('css/styletemplate.css')}}" rel="stylesheet">
<link href="{{asset('css/fonttemplate.css')}}" rel="stylesheet">

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        {{-- <div class="panel-heading">Dashboard</div> --}}
          <div class="panel-body">
            @if (session('msg')) 
              <div class="alert alert-success"> {{ session('msg') }} </div>
            @endif
            <h2 class="text-center">Selecciona Tu plantilla</h2>

  <div class="full-wrapper clearfix">
    <div class="container">
      <div class="sectionTitle text-center"><span class="number">4</span><h2>Plantillas <br>(proximamente muchas más)</h2>
    </div>
      <div class="row text-center">
        <div class="col-sm-6 col-xs-12">
          <div class="box box1">
            <div class="box1-inner">
              <a href="{{route('dashboard.setTemplate','Fashion')}}"></a>
            </div>
            <h3>Fashion</h3>
          </div>
        </div>
        <div class="col-sm-6 col-xs-12">
          <div class="box box2">
            <div class="box4-inner">
              <a href="{{route('dashboard.setTemplate','Electronic')}}"></a>
            </div>
            <h3>Electronic</h3>
          </div>
        </div>

        <div class="col-sm-6 col-xs-12">
          <div class="box box3">
            <div class="box3-inner">
              <a href="{{route('dashboard.setTemplate','Rustica')}}"></a>
            </div>
            <h3>Rustica</h3>
          </div>
        </div>

        <div class="col-sm-6 col-xs-12">
          <div class="box box2">
            <div class="box2-inner">
              <a href="{{route('dashboard.setTemplate','Stylo')}}"></a>
            </div>
            <h3>Stylo</h3>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="feature clearfix">
    <div class="container">
      <div class="sectionTitle text-center">
        <h2>Proximas Caracteristicas</h2>
      </div>
      <div class="row text-center">
        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-1.png')}}" alt="Image">
            </span>
            <p>Multiples Cabeceras</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-2.png')}}" alt="Image">
            </span>
            <p>Mega Menu</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-3.png')}}" alt="Image">
            </span>
            <p>Mejoras en la compatibilidad movil</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-4.png')}}" alt="Image">
            </span>
            <p>100+ de icons e imagenes</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-5.png')}}" alt="Image">
            </span>
            <p>Optimizacion de buscadores</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-6.png')}}" alt="Image">
            </span>
            <p>Reporte de Ventas</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-7.png')}}" alt="Image">
            </span>
            <p>Vista Rapida</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-8.png')}}" alt="Image">
            </span>
            <p>Compras</p>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-9.png')}}" alt="Image">
            </span>
            <p>Edicion de Elementos</p>
          </div>
        </div>
        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center">
            <span>
              <img src="{{asset('images/icon-10.png')}}" alt="Image">
            </span>
            <p>Personalizacion de paginas</p>
          </div>
        </div>
        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center"><span><img src="{{asset('images/icon-11.png')}}" alt="Image"></span><p>MultiIdioma</p>
          </div>
        </div>
        <div class="col-sm-3 col-xs-6">
          <div class="feature-box text-center"><span><img src="{{asset('images/icon-12.png')}}" alt="Image"></span><p>Vista en Construccion</p></div>
        </div>
      </div>

    </div>
  </div>
                      



      </div>
    </div>
  </div>
</div>
@endsection