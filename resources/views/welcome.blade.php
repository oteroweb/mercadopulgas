@extends('layouts.app')

@section('content')

<section class="section banner">
    <div>
        <img src="img/mercadopulgas-main-banner.png">
    </div>
</section>
<section class="section categories">
	<div class="container">
	    <h2 class="categories__title-section">¿Qué estás buscando?</h2>
    	<p class="categories__title-list">Categorias</p>
	    <ul class="categories-list">
		    @foreach ($categories as $category)
		    <li>
		    	<a href="{{route('category',['category'=>$category->slug])}} ">{{ $category->name }}</a>
		    </li> 
		    @endforeach
	    </ul>
	</div>
</section>
<section class="section">
	<div class="container">
	    <h2 class="categories__title-section">¿ Qué es Mercadopulgas ?</h2>
		<div class="row">
			<div class="col-md-6">
    			<img class="img-responsive" src="{{ asset('images/shopping-cart.png') }}" />
			</div>
			<div class="col-md-6">
				<h2>
				Mercadopulgas Es el mayor Centro comercial virtual de Venezuela, donde se reunen todas las tiendas creadas con <b>OWShop</b>, El generador de tiendas virtuales que Oteroweb Tiene para ti
				</h2>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">
	    <h2 class="categories__title-section">¿ Que puedes hacer en <b>MercadoPulgas</b> ?</h2>
		<div class="row">
			<div class="col-md-8">
				<div class="list-group">
				  <a class="list-group-item list-group-item-info">
				    <h4 class="list-group-item-heading">Comprar Articulos nuevos y usados</h4>
				    <p class="list-group-item-text">Buscando el precio ideal para ti, En mercadopulgas consigues infinidad de ofertas</p>
				  </a>
				</div>
    			<div class="list-group">
				  <a class="list-group-item list-group-item-info">
				    <h4 class="list-group-item-heading">Vender articulos desde Mercadopulgas</h4>
				    <p class="list-group-item-text">Te permite tener en tiempo real los productos de tu tienda y venderlos</p>
				  </a>
				</div>
				<div class="list-group">
				  <a class="list-group-item list-group-item-info">
				    <h4 class="list-group-item-heading">Tener tu tienda en linea con simples pasos</h4>
				    <p class="list-group-item-text">En solo 5 minutos tienes tu tienda 100% funcional</p>
				  </a>
				</div>
				<div class="list-group">
				  <a class="list-group-item list-group-item-info">
				    <h4 class="list-group-item-heading">Manejar inventario</h4>
				    <p class="list-group-item-text">Manejar lotes, repotres de Ventas</p>
				  </a>
				</div>
				<div class="list-group">
				  <a class="list-group-item list-group-item-info">
				    <h4 class="list-group-item-heading">Gestionar vendedores y gerentes en tus tiendas</h4>
				    <p class="list-group-item-text">Manejar una tienda no es trabajo de una persona, Gestiona tus Vendedores y Gerentes</p>
				  </a>
				</div>
				<div class="list-group">
				  <a class="list-group-item list-group-item-info">
				    <h4 class="list-group-item-heading">Facturar y Presupuestar</h4>
				    <p class="list-group-item-text">Puedes emitir facturas y presupuestar a tus clientes</p>
				  </a>
				</div>

			</div>
			<div class="col-md-4">
    			<img class="img-responsive" src="{{ asset('images/doubtmen.png') }}" />
			</div>
		</div>
	</div>
</section>
<section class="section register">
	<div class="container">
		<div class="register__content">
			<h2 class="register__text">
				¿Deseas ser parte <br> de <strong>MercadoPulgas</strong>?
			</h2>
			<div class="btn btn-primary register__button ">
				<a class="register__anchor"	href="{{route('register')}}">¡ Registrate Gratis !</a>
			</div>
		</div>
	</div>
</section>


<section class="section payments">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 payments__wrapper">
				<div class="card">
					<div class="card__header">
					<i class="glyphicon glyphicon-credit-card"></i>
						
					</div>
					<div class="card__text">
						Usa nuestro novedoso sistema de Pagos OnLine de forma segura y con el medio de pago de tu preferencia.
					</div>
					<div class="card__img">
						<img src="img/mercadopulgas-payments-visa.png">
					</div>
				</div>
				<div class="card">
					<div class="card__header">
					<i class="glyphicon glyphicon-phone-alt"></i>
						
					</div>
					<div class="card__text">
						Contamos con un servicio de atención las 24 hrs, recibimos tus dudas, sugerencias. Estamos disponibles para ti.
					</div>
					<div class="card__img">
						<img src="img/mercadopulgas-payments-call.png">
					</div>
				</div>
				<div class="card">
					<div class="card__header">
					<i class="glyphicon glyphicon-usd"></i>
						
					</div>
					<div class="card__text">
						Las comisiones de MercadoPulgas son las más bajas. Rentabilidad y buen servicio al menor costo.
					</div>
					<div class="card__img">
						<img src="img/mercadopulgas-payments-cash.png">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
<style type="text/css">
.glyphicon{font-size: 2em; padding:0.1em 0em;}
a.register__anchor{color:white;}
</style>
