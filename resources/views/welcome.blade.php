@extends('layouts.app')

@section('content')
<section class="section banner">
    <div>
        <img src="img/mercadopulgas-main-banner.png">
    </div>
</section>
<section class="section categories">
	<div class="container">
	    <h2 class="categories__title-section">¿Qué estás buscando?</h2>
    	<p class="categories__title-list">Categorias</p>
	    <ul class="categories-list">
		    @foreach ($categories as $category)
		    <li>
		    	<a href="{{route('category',['category'=>$category->slug])}} ">{{ $category->name }}</a>
		    </li> 
		    @endforeach
	    </ul>
	</div>
</section>

<section class="section register">
	<div class="container">
		<div class="register__content">
			<h2 class="register__text">
				¿Deseas ser parte <br> de <strong>MercadoPulgas</strong>?
			</h2>
			<div class="btn btn-primary register__button ">
				Regístrate ¡gratis!
			</div>
		</div>
	</div>
</section>

<section class="section payments">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 payments__wrapper">
				<div class="card">
					<div class="card__header">
					<i class="glyphicon glyphicon-credit-card"></i>
						
					</div>
					<div class="card__text">
						Usa nuestro novedoso sistema de Pagos OnLine de forma segura y con el medio de pago de tu preferencia.
					</div>
					<div class="card__img">
						<img src="img/mercadopulgas-payments-visa.png">
					</div>
				</div>
				<div class="card">
					<div class="card__header">
					<i class="glyphicon glyphicon-phone-alt"></i>
						
					</div>
					<div class="card__text">
						Contamos con un servicio de atención las 24 hrs, recibimos tus dudas, sugerencias. Estamos disponibles para ti.
					</div>
					<div class="card__img">
						<img src="img/mercadopulgas-payments-call.png">
					</div>
				</div>
				<div class="card">
					<div class="card__header">
					<i class="glyphicon glyphicon-usd"></i>
						
					</div>
					<div class="card__text">
						Las comisiones de MercadoPulgas son las más bajas. Rentabilidad y buen servicio al menor costo.
					</div>
					<div class="card__img">
						<img src="img/mercadopulgas-payments-cash.png">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
<style type="text/css">.glyphicon{font-size: 2em; padding:0.1em 0em;}</style>
