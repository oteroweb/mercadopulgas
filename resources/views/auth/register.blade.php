@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">Registro</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<div class="btn-group btn-group-justified" >
						  <a class="btn btn-default" href="{{route('registerType',['type'=> 't1'])}}">
						   <!-- mejorar lectura textos -->
								<img src="{{@asset('images/vendedor.png')}}" class="img-responsive" alt="">
									<h2>Comprador/Vendedor  </h2> <br> <p class="text-space">"Puedes Comprar y Vender articulos directamente desde MercadoPulgas"</p>
						  </a>
						  <a class="btn btn-default" href="{{route('registerType',['type'=> 't2'])}}">
						   <!-- mejorar lectura textos -->
								<img src="{{@asset('images/ecommerce.png')}}" class="img-responsive" alt="">
									<h2> Dueño de Tienda  </h2> <br> <p class="text-space">"Puedes comprar y vender desde tu tienda OWSHOP y desde Mercadopulgas"</p>
						  </a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
<script  src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<style>p.text-space {
    white-space: initial;
}</style>