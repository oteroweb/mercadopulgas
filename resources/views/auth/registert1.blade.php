@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading">Registro</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
							<div class="form-group noshow text-center{{ $errors->has('typeUser') ? ' has-error' : '' }}">
								<label>¿Tipo de usuario?</label>
           			<label class="radio-inline"><input required type="radio" name="typeUser" {{($type == "0") ? 'checked' : ""}} value="0">sin tienda</label>
           			<label class="radio-inline"><input required type="radio" name="typeUser" {{($type == "1") ? 'checked' : ""}} value="1">con tienda</label>
							@if ($errors->has('typeUser'))
									<span class="help-block">
										<strong>{{ $errors->first('typeUser') }}</strong>
									</span>
								@endif 
						</div>

						<div class="form-group text-center{{ $errors->has('typeClient') ? ' has-error' : '' }}">
								<label>¿Eres persona natural ó juridica?</label>
           			<label class="radio-inline"><input required type="radio" name="typeClient" value="0" @if(!old('typeClient')) checked @endif>Persona Natural</label>
           			<label class="radio-inline"><input required type="radio" name="typeClient" value="1" @if(old('typeClient')) checked @endif>Persona Juridica</label>
							@if ($errors->has('typeClient'))
									<span class="help-block">
										<strong>{{ $errors->first('typeClient') }}</strong>
									</span>
								@endif 
						</div>


							<div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
							<label for="document" class="col-md-4 control-label">Documento de Identidad  
							</label>

							<div class="col-md-6">
								<input id="document"  type="text" placeholder="Ej: V000000000"  class="form-control" name="document" value="{{ old('document') }}" required autofocus>
								<span id='error-document' style="display:none" class='help-block warning'><strong>El Documento no es valido debe tener el siguiente formato V00000000</strong></span>
								@if ($errors->has('document'))
									<span class="help-block"><strong>{{ $errors->first('document') }}</strong></span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-md-4 control-label">Nombre Completo</label>
							<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
								@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('address_home') ? ' has-error' : '' }}">
							<label for="address_home" class="col-md-4 control-label">Direccion</label>
							<div class="col-md-6"> 
								<input id="address_home" type="text" class="form-control" name="address_home" value="{{ old('address_home') }}" required autofocus>
								@if ($errors->has('address_home'))
									<span class="help-block">
										<strong>{{ $errors->first('address_home') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<label for="phone" class="col-md-4 control-label">Telefono (movil o local)</label>
							<div class="col-md-6">
								<input id="phone"  type="number" placeholder="Ej: 04160000000"  class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>
								@if ($errors->has('phone'))
									<span class="help-block">
										<strong>{{ $errors->first('phone') }}</strong>
									</span>
								@endif
							</div>
						</div>
					<!--hasta aqui lo nuevo -->
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">Correo Electronico</label>

							<div class="col-md-6">
								<input id="email"  placeholder="Ej: correo@prueba.com" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
								<span id='error-email' style="display:none" class='help-block warning'><strong>El correo no es valido debe ser con el siguiente formato info@oteroweb.com</strong></span>

								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-md-4 control-label">Contraseña</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required>

								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="password-confirm" class="col-md-4 control-label" value="{{ old('password-confirm') }}">Repetir Contraseña</label>

							<div class="col-md-6">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>
						</div>


							@if($type == '1')
						<div class="form-group{{ $errors->has('name_shop') ? ' has-error' : '' }}">
							<label for="name_shop" class="col-md-4 control-label">Nombre de tu tienda</label>
							<div class="col-md-6">
								<input id="name_shop" type="text" class="form-control" name="name_shop" value="{{ old('name_shop') }}" required autofocus>
								@if ($errors->has('name_shop'))
									<span class="help-block">
										<strong>{{ $errors->first('name_shop') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('name_shop_slug') ? ' has-error' : '' }}">
							<label for="name_shop_slug" class="col-md-4 control-label">Direccion de tu tienda</label>
							<div class="col-md-6">
								<input id="name_shop_slug" type="text" class="form-control" name="name_shop_slug" value="{{ old('name_shop_slug') }}" required autofocus>
								<input id="slugify" type="hidden" class="form-control" name="slugify" value="{{ old('slugify') }}" required autofocus>
								@if ($errors->has('name_shop_slug'))
									<span class="help-block">
										<strong>{{ $errors->first('name_shop_slug') }}</strong>
									</span>
								@endif
							</div>
						</div>

<div class="form-group{{ $errors->has('name_shop_slug') ? ' has-error' : '' }}">
							<label for="name_shop_slug" class="col-md-4 control-label">Direccion Web tienda</label>
							<div class="col-md-6">
								<span id="url_shop" class="help-block" > <strong>{{ (old('slugify') != null) ? "https://".old('slugify').".mercadopulgas.com.ve" : "" }}</strong> </span>
								@if ($errors->has('name_shop_slug'))
									<span class="help-block">
										<strong>{{ $errors->first('name_shop_slug') }}</strong>
									</span>
								@endif
							</div>
						</div>
			
		
							@endif
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Registar 
								</button>
							</div>
						</div>
 
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
<script  src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>

<script>
var url_shop = 'http://www.mercadopulgas.com.ve/tienda/'

	$( window ).on( "load", function() {
	console.log( "window loaded" );
				var rif = /^([VEJGP])([0-9]{5,8})([0-9]{1})$/i;
				var email = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;


 $("#document, #email").blur(function () {
        var VAL = this.value;
 	
 	$("#document").blur(function () {
        var VAL = this.value;
        if (!rif.test(VAL)) { 
        	$('#document').parent().attr("class",'col-md-6 has-error');
					$('#error-document').show();
        } else {
					$('#document').parent().attr("class",'col-md-6');
					$('#error-document').hide();
        }
    });
	$("#email").blur(function () {
        var VAL = this.value;
       	if (!email.test(VAL)) { 
        	$('#email').parent().attr("class",'col-md-6 has-error');
					$('#error-email').show();
        } else {
					$('#email').parent().attr("class",'col-md-6');
					$('#error-email').hide();
        }
	  });
	       
        if (rif.test($("#document").val()) && email.test($("#email").val()) ) {
          $(':input[type="submit"]').prop('disabled', false);
        }
  });

 $('#name_shop').on('keyup',function(){
   $('#name_shop_slug').val(slugify($(this).val()));  
   $('#slugify').val(slugify($(this).val()));  
   $('#url_shop.help-block').show();  
   $('#url_shop>strong').text('https://'+slugify($(this).val())+".mercadopulgas.com.ve");  
  });

  $('#name_shop_slug').on('keyup',function(){
   $('#slugify').val(slugify($(this).val()));  
   $('#url_shop').show();  
   $('#url_shop>strong').text('https://'+slugify($(this).val())+".mercadopulgas.com.ve");  
  });
});



	// change url
</script>

<script>
  
  function slugify (text) {
  const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
  // const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
  const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh______'
  const p = new RegExp(a.split('').join('|'), 'g')

  return text.toString().toLowerCase()
	.replace(/\s+/g, '_')           // Replace spaces with -
	.replace(p, c =>
		b.charAt(a.indexOf(c)))     // Replace special chars
	.replace(/&/g, '_and_')         // Replace & with 'and'
	.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
	.replace(/\-\-+/g, '_')         // Replace multiple - with single -
	.replace(/^-+/, '')             // Trim - from start of text
	.replace(/-+$/, '')             // Trim - from end of text
  }

</script>
<style>

p.text-space {white-space: initial; }
.radio-inline{font-size: 2rem;}
#app > div > div > div > div > div.panel-body > form>div> label{font-size:2rem;}
.noshow{display:none;}
#url_shop { color: blue;
    font-size: 2rem;}
</style>