@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">Register</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="is_buy" {{ old('is_buy') ? 'checked' : '' }}> Deseo Comprar
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="is_sell" {{ old('is_sell') ? 'checked' : '' }}> Deseo Vender
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="is_shop" {{ old('is_shop') ? 'checked' : '' }}> Deseo tener mi tienda
									</label>
								</div>
							</div>
						</div>
						<div class="form-group"><div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="is_ticket" {{ old('is_ticket') ? 'checked' : '' }}> Deseo mi sistema administrativo y facturar
									</label>
								</div>
							</div>
						</div>
						<div class="radio">
						  <label><input type="radio" name="optradio"  required value="PN">Persona Natural</label>
						</div>
						<div class="radio">
						  <label><input type="radio" name="optradio" value="PJ">Persona Juridica</label>
						</div>
						<div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
							<label for="document" class="col-md-4 control-label">Documento de Identidad  <button id="botonconsultar"> consultar </button></label>

							<div class="col-md-6">
								<input id="document"  type="text" class="form-control" name="document" value="{{ old('document') }}" required autofocus>

								@if ($errors->has('document'))
									<span class="help-block">
										<strong>{{ $errors->first('document') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-md-4 control-label">Nombre Completo</label>
							<div class="col-md-6">
							<input id="name" readonly type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
								@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('name_shop') ? ' has-error' : '' }}">
							<label for="name_shop" class="col-md-4 control-label">Nombre de tu tienda</label>

							<div class="col-md-6">
								<input id="name_shop" type="text" class="form-control" name="name_shop" value="{{ old('name_shop') }}" required autofocus>

								@if ($errors->has('name_shop'))
									<span class="help-block">
										<strong>{{ $errors->first('name_shop') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('name_shop_slug') ? ' has-error' : '' }}">
							<label for="name_shop_slug" class="col-md-4 control-label">Direccion de tu tienda</label>

							<div class="col-md-6">
								<input readonly id="name_shop_slug" type="text" class="form-control" name="name_shop_slug" value="{{ old('name_shop_slug') }}" required autofocus>

								@if ($errors->has('name_shop_slug'))
									<span class="help-block">
										<strong>{{ $errors->first('name_shop_slug') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('address_home') ? ' has-error' : '' }}">
							<label for="address_home" class="col-md-4 control-label">Direccion</label>

							<div class="col-md-6">
								<input id="address_home" type="text" class="form-control" name="address_home" value="{{ old('address_home') }}" required autofocus>

								@if ($errors->has('address_home'))
									<span class="help-block">
										<strong>{{ $errors->first('address_home') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="phone" class="col-md-4 control-label">Telefono (movil o local)</label>
							<div class="col-md-6">
								<input id="phone"  type="number" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>
								@if ($errors->has('phone'))
									<span class="help-block">
										<strong>{{ $errors->first('phone') }}</strong>
									</span>
								@endif
							</div>
						</div>
					<div class="form-group{{ $errors->has('template') ? ' has-error' : '' }}">
							<label for="template" class="col-md-4 control-label">plantilla de la tienda(proximamente un editor)</label>

							<div class="col-md-6">
								<select id="themes" type="text" class="form-control" name="template" value="{{ old('template') }}" required autofocus> </select>    

								@if ($errors->has('template'))
									<span class="help-block">
										<strong>{{ $errors->first('template') }}</strong>
									</span>
								@endif
							</div>
						</div>
<!--hasta aqui lo nuevo -->
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">Correco Electronico</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-md-4 control-label">Contraseña</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="password-confirm" class="col-md-4 control-label">Repetir Contraseña</label>

							<div class="col-md-6">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Registar 
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
<script  src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript"> 
var url_shop = 'http://www.mercadopulgas.com.ve/tienda/'
  $( document ).ready(function() { });
  $( window ).on( "load", function() {
	console.log( "window loaded" );
	$("#document").on("change",function(){
	  $("#name").val(""); });
	$("#botonconsultar").on('click', function cambio(event){
	  event.preventDefault();
	  if ($("[name=optradio]:checked").length == 0){ alert ('Debe Indicar si es una Persona Juridica o Natural'); return; }
	  else if($("#document").val() == "" ) {
		alert("El campo Documento de identidad es requerido"); return; }
	  else if($("#document").val().length < 5 ) {
		alert("Ingrese el documento de identidad correctamente"); return; }
	  else if(!["v","V","e","E","j","J","G","g"].includes($("#document").val().charAt(0)) ) { alert("El formato de su Documento es incorrecto debe ser cedula = V12345678 y para el RIF J123456789 "); return; }
	  else { 
		if ($("[name=optradio]:checked").val() == "PN")
		  { var nacionalidad = $("#document").val().toUpperCase().charAt(0);
			var cedula = $("#document").val().substring(1);
			$.get( "api/consulta/cedula/"+nacionalidad+"/"+cedula, function( data ) {
			  if (data.error == true ) {
				if(data.descripcion.includes("Resolving timed")) { alert("Error de conexion intente nuevamente");
				  $("#name").val(""); 
				}
				if(data.descripcion.includes("El usuario no se encuentra inscrito en el registro electoral")) {
				  alert("El documento de identidad no existe verifique y consulte nuevamente "); 
					$("#name").val("");
					}
				}
				else if (data.error == false)
				  if ( data.objecion && data.objecion.includes("FALLECIDO"))
					{ alert("El documento de identidad corresponde a una persona fallecida no puede usar este documento");  $("#name").val("");
					  $("#name").val("");
					}
				  else { console.log(data); $("#name").val(data.nombre); }
				},"json");
			  }
			if ($("[name=optradio]:checked").val() =="PJ"){
				var nacionalidad = $("#document").val().toUpperCase().charAt(0);
				var cedula = $("#document").val().substring(1);
			  $.get( "api/consulta/rif/"+nacionalidad+"/"+cedula, function( data ) {
				if (data.descripcion && data.descripcion.includes("Resolving timed")){
					if(true ) { alert("Error de conexion intente nuevamente"); }
					return; }
				else if(data.error) {
					alert("El Rif No Existe"); return;
				} 
				else { $("#name").val(data.nombre); }
			},"json");
			} 
		  }
		}
	  );
	$.getJSON("api/getThemes", function (data) {
	  var themes = data;
	  var select = $("#themes");
	  themes.forEach( function(value, index){
		  text = value.substr( 0, 1 ).toUpperCase() + value.substr( 1 );
		  if(text == "Mercadopulgas") { select.append($("<option selected/>").val(value).text(text)); }
		  else { select.append($("<option/>").val(value).text(text)); }
	  });
	  select.on('change',function(){ 
			// $(".estilo").attr('href',$(".estilo").attr('href') == 'http://mercadopulgas.com.ve/storage/themes/'+select.value()+'.css' ? '' : 'http://mercadopulgas.com.ve/storage/themes/mercadopulgas.css');
			$(".estilo").attr('href','http://mercadopulgas.com.ve/storage/themes/'+$("#themes").val()+'.css');
	  });
	}, "json").fail(function(){
	// alerta de fallido impotante4 aplicar a verificacion
	// $(".alert").toggleClass("alert-info alert-danger");
	// $(".alert h4").text("Failure!");
	});
  setInterval(function(){ }, 500);
  $('#name_shop').on('keyup',function(){
   $('#name_shop_slug').val(url_shop+slugify($(this).val()));  
  });

  });
// j295942486

</script>
<script>
  
  function slugify (text) {
  const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
  // const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
  const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh______'
  const p = new RegExp(a.split('').join('|'), 'g')

  return text.toString().toLowerCase()
	.replace(/\s+/g, '_')           // Replace spaces with -
	.replace(p, c =>
		b.charAt(a.indexOf(c)))     // Replace special chars
	.replace(/&/g, '_and_')         // Replace & with 'and'
	.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
	.replace(/\-\-+/g, '_')         // Replace multiple - with single -
	.replace(/^-+/, '')             // Trim - from start of text
	.replace(/-+$/, '')             // Trim - from end of text
  }

</script>