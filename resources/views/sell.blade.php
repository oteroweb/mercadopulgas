@extends('layouts.app')

@section('content')

<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Dashboard</div>
                
                <div class="panel-body">
                    

            
                </div>
                <div> <h1 class="text-center">Publicar Producto</h1> 
                 


                   @if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
					@endif

					@if (session('msg'))
					    <div class="alert alert-success">
					        {{ session('msg') }}
					    </div>
					@endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('sell.publish') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- Select: Category -->
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-4 control-label">Categoria</label>

                            <div class="col-md-6">
                                 <select id="category" type="category" class="form-control" name="category" value="{{ old('category') }}" required autofocus>
								@foreach ($categories as $category)
                            		 <option value="{{$category->id}}">{{ $category->name }}</option>
                        		@endforeach
									  </select>
                                @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: Category> -->
                        <!-- Select: Foto -->
                        <div class="form-group{{ $errors->has('picture1') ? ' has-error' : '' }}">
                            <label for="picture1" class="col-md-4 control-label">Foto 1 </label>

                            <div class="col-md-6">
                                 <input id="picture1" type="file" class="form-control" name="picture1" value="{{ old('picture1') }}" accept="image/*" autofocus>
                                @if ($errors->has('picture1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('picture1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: Foto> -->

                        <!-- Select: Promotional Title -->
                        <div class="form-group{{ $errors->has('promotional_title') ? ' has-error' : '' }}">
                            <label for="promotional_title" class="col-md-4 control-label">Titulo Promocional (Titulo con el cual Sera Visto tu articulo) </label>

                            <div class="col-md-6">
                                 <input id="promotional_title" type="text" class="form-control" name="promotional_title" value="{{ old('promotional_title') }}" accept="image/*" autofocus required>
                                @if ($errors->has('promotional_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('promotional_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: Promotional Title> -->

                        <!-- Select: Promotional subtitle -->
                        <div class="form-group{{ $errors->has('promotional_subtitle') ? ' has-error' : '' }}">
                            <label for="promotional_subtitle" class="col-md-4 control-label">Subtitulo  (subtitulo con el cual Sera Visto tu articulo) </label>

                            <div class="col-md-6">
                                 <input id="promotional_subtitle" type="text" class="form-control" name="promotional_subtitle" value="{{ old('promotional_subtitle') }}" autofocus required>
                                @if ($errors->has('promotional_subtitle'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('promotional_subtitle') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: Promotional subtitle> -->

 						<!-- Select: name -->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre del articulo(Nombre del articulo en el inventario) </label>

                            <div class="col-md-6">
                                 <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"   required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: name> -->

                        <!-- Select: price -->
                        <div class="form-group{{ $errors->has('disponibility') ? ' has-error' : '' }}">
                            <label for="disponibility" class="col-md-4 control-label">Disponibilidad (Stock) </label>

                            <div class="col-md-6">
                                 <input id="disponibility" type="number" step="0.01" class="form-control" name="disponibility" value="{{ old('disponibility') }}"  required autofocus >
                                @if ($errors->has('disponibility'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('disponibility') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: price> -->

						<!-- Select: price -->
                        <div class="form-group{{ $errors->has('price_sell') ? ' has-error' : '' }}">
                            <label for="price_sell" class="col-md-4 control-label">Precio de Venta </label>

                            <div class="col-md-6">
                                 <input id="price_sell" type="number" step="0.01" class="form-control" name="price_sell" value="{{ old('price_sell') }}" required autofocus >
                                @if ($errors->has('price_sell'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price_sell') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: price> -->
						<!-- Select: description -->
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripcion </label>

                            <div class="col-md-6">
                                 <textarea  id="description" class="form-control" name="description" required value="{{ old('description') }}" autofocus  cols="40" rows="5"></textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: description> -->
						<!-- Select: is nuevo -->

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label> 
                                       <input type="checkbox" name="is_new" {{ old('is_new') ? 'checked' : '' }} value="1">  ¿Es nuevo?
                                    </label> 
                                </div>
                            </div>
                        </div>
                        <!-- </Select: published_mp> -->

						<!-- Select: is nuevo -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label> 
                                       <input type="checkbox" name="publishmp" {{ old('publishmp') ? 'checked' : '' }} value="1">  ¿Publicar en Mercadopulgas?
                                    </label> 
                                </div>
                            </div>
                        </div>
                        <!-- </Select: is nuevo> -->
                        <!-- Select: published_shop -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label> 
                                       <input type="checkbox" name="published_shop" {{ old('publish') ? 'checked' : '' }} value="1">  ¿Publicar en mi tienda?
                                    </label> 
                                </div>
                            </div>
                        </div>
                        <!-- </Select: is nuevo> -->

                        <!-- Select: published_software -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label> 
                                       <input type="checkbox" name="owsoftware" {{ old('owsoftware') ? 'checked' : '' }} value="1">  ¿Agregar a mi sistema Administrativo?
                                    </label> 
                                </div>
                            </div>
                        </div>
                        <!-- </Select: is nuevo> -->

                       
                       	


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Añadir a inventario / Vender
                                </button>

                    
                            </div>
                        </div>
                    </form>
                
                </div>	
            </div>
        </div>
    </div>
</div>



@endsection