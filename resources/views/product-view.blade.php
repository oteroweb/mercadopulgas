  @if( is_null($shop) )
    sin tienda
    @foreach ($products as $product)
    <div class="col-md-3 product-list-group">
      <a href="{{route('product',['category'=>  $current_category,'product'=>$product->slug])}}">
         <h4 class="product-list-title"> {{ $product->promotional_title }} </h4>
        <h5 class="product-list-subtitle"> {{$product->promotional_subtitle}}</h5>
        <img class="product-list-img" src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> 
        <p class="product-list-price"> {{$product->price_sell}} Bs.F. </p>
      </a>
    </div>
    @endforeach
    @else
    con tienda
    @foreach ($products as $product)
    <div class="col-md-3 product-list-group">
      <a href="{{route('product.shop',['category'=>  $current_category,'product'=>$product->slug,'shop'=> $shop])}} ">
        <h4 class="product-list-title"> {{ $product->promotional_title }} </h4>
        <h5 class="product-list-subtitle"> {{$product->promotional_subtitle}}</h5>
        <img class="product-list-img" src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> 
        <p class="product-list-price"> {{$product->price_sell}} Bs.F. </p>
      </a>
    </div>
      @endforeach
    @endif     