@extends('layouts.app')
@section('content')
<link class="estilo" href="{{asset('storage/themes/')}}/{{(Auth::user()->template != '') ? Auth::user()->template : ''}}.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<section class="content">
			<h1>Mis Productos</h1>
			<div class="col-md-12 ">
				@if (count($errors) > 0)
					<div class="alert alert-danger"> 
						<ul>  
						@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach
					</ul>
				  </div>
					@endif
					@if (session('status')) <div class="alert alert-success">{{ session('status') }} </div> @endif
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="pull-right">
							Disponible en 
							<div class="btn-group">
								<button type="button" class="btn btn-success btn-filter" data-target="publishmp">Mercadopulgas </button>
								<button type="button" class="btn btn-warning btn-filter" data-target="owsoftware">Sistema Adm.</button>
								<button type="button" class="btn btn-danger btn-filter" data-target="published_shop">Tienda Virtual</button>
								<button type="button" class="btn btn-default btn-filter" data-target="all">Todos</button>
							</div>
						</div>
						<div class="table-container table-responsive">



							<table class="table table-product">
								<thead class="text-center"> 
									<tr> 
										<td>Selecciona</td>
										<td>Foto</td>
										<td>Descripcion</td>
										<td>Disponible en</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody>
									@foreach ($products as $product)

									<tr  class="product-unit" 		
										

									data-status= "['{{($product->publishmp == true) ? 'publishmp' : '' }}','{{($product->published_shop == true) ? 'published_shop' : '' }}','{{($product->owsoftware == true) ? 'owsoftware' : '' }}']">
										
										<td class="col-md-1">
											<div class="ckbox">
												<input type="checkbox" id="checkbox1">
												<label for="checkbox1"></label>
											</div>
										</td>
										<td class="col-md-2">
											<img class="product-list-img" src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> 
										</td>
										<td class="col-md-5">
											<h4>{{$product->promotional_title}}</h4>
											<h4>{{$product->promotional_subtitle}}</h4>
											<h4>{{$product->price_sell}}</h4>
											<h4>{{$product->disponibility}}</h4>
										</td>
										<td class="col-md-2" >
											<h4><button type="button" class="btn btn-{{($product->publishmp == true) ? 'success' : 'default' }} publishmp-button">Mercadopulgas <span class="badge">{{($product->publishmp == true) ? 'Si' : 'No' }}</span></button></h4>

											<h4><button type="button" class="btn btn-{{($product->owsoftware == true) ? 'success' : 'default' }} owsoftware-button">OwSoftware <span class="badge">{{($product->owsoftware == true) ? 'Si' : 'No' }}</span></button></h4>
											<h4><button type="button" class="btn btn-{{($product->published_shop == true) ? 'success' : 'default' }} published_shop-button">OwShop <span class="badge">{{($product->published_shop == true) ? 'Si' : 'No' }}</span></button></h4>
										</td>
										<td class="col-md-2">
											<a href="{{route('product',['category' => $product->category->slug, 'product' => $product->slug])}}">
													<h4 class="btn btn-success">
														<i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" title="Vista producto en Mercadopulgas"></i>
													</h4>
											</a>
											<br>	
											<a href="{{route('product.shop',['category' =>$product->category->slug, 'product' => $product->slug,'shop'=>$product->user->name_shop_slug])}}">
												<h4 class="btn btn-success">
													<i class="glyphicon glyphicon-shopping-cart" data-toggle="tooltip" data-placement="top" title="Vista producto en Mi tienda"></i>
												</h4>
											</a>
											<br>	
											<!-- /tienda/{shop}/{category}/{product} -->
											<!-- categoria/{category}/{product} -->
											<a href="{{route('dashboard.products.edit',['slug' => $product->slug])}} ">
												<h4 class="btn btn-success">
													<i class="glyphicon glyphicon-pencil" data-placement="top" data-toggle="tooltip" title="Editar"></i>
												</h4>
											</a>	
											<br>
											<!-- <a href=""><h4 class="btn btn-success"><a href=""><i class="glyphicon glyphicon-trash"></i> </h4> <br></a>	 -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
			</div>
		</section>
		
	</div>
</div>
@endsection

<style type="text/css">
	/*    --------------------------------------------------
	:: General
	-------------------------------------------------- */
	body {
		font-family: 'Open Sans', sans-serif;
		color: #353535;
	}
	.content h1 {
		text-align: center;
	}
	.content .content-footer p {
		color: #6d6d6d;
		font-size: 12px;
		text-align: center;
	}
	.content .content-footer p a {
		color: inherit;
		font-weight: bold;
	}

/*	--------------------------------------------------
	:: Table Filter
	-------------------------------------------------- */
	.panel {
		border: 1px solid #ddd;
		background-color: #fcfcfc;
	}
	.panel .btn-group {
		margin: 15px 0 30px;
	}
	.panel .btn-group .btn {
		transition: background-color .3s ease;
	}
	.table-filter {
		background-color: #fff;
		border-bottom: 1px solid #eee;
	}
	.table-filter tbody tr:hover {
		cursor: pointer;
		background-color: #eee;
	}
	.table-filter tbody tr td {
		padding: 10px;
		vertical-align: middle;
		border-top-color: #eee;
	}
	.table-filter tbody tr.selected td {
		background-color: #eee;
	}
	.table-filter tr td:first-child {
		width: 38px;
	}
	.table-filter tr td:nth-child(2) {
		width: 35px;
	}
	.ckbox {
		position: relative;
	}
	.ckbox input[type="checkbox"] {
		opacity: 0;
	}
	.ckbox label {
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
	.ckbox label:before {
		content: '';
		top: 1px;
		left: 0;
		width: 18px;
		height: 18px;
		display: block;
		position: absolute;
		border-radius: 2px;
		border: 1px solid #bbb;
		background-color: #fff;
	}
	.ckbox input[type="checkbox"]:checked + label:before {
		border-color: #2BBCDE;
		background-color: #2BBCDE;
	}
	.ckbox input[type="checkbox"]:checked + label:after {
		top: 3px;
		left: 3.5px;
		content: '\e013';
		color: #fff;
		font-size: 11px;
		font-family: 'Glyphicons Halflings';
		position: absolute;
	}
	.table-filter .star {
		color: #ccc;
		text-align: center;
		display: block;
	}
	.table-filter .star.star-checked {
		color: #F0AD4E;
	}
	.table-filter .star:hover {
		color: #ccc;
	}
	.table-filter .star.star-checked:hover {
		color: #F0AD4E;
	}
	.table-filter .media-photo {
		width: 35px;
	}
	.table-filter .media-body {
		display: block;
		/* Had to use this style to force the div to expand (wasn't necessary with my bootstrap version 3.3.6) */
	}
	.table-filter .media-meta {
		font-size: 11px;
		color: #999;
	}
	.table-filter .media .title {
		color: #2BBCDE;
		font-size: 14px;
		font-weight: bold;
		line-height: normal;
		margin: 0;
	}
	.table-filter .media .title span {
		font-size: .8em;
		margin-right: 20px;
	}
	.table-filter .media .title span.pagado {
		color: #5cb85c;
	}
	.table-filter .media .title span.pendiente {
		color: #f0ad4e;
	}
	.table-filter .media .title span.cancelado {
		color: #d9534f;
	}
	.table-filter .media .summary {
		font-size: 14px;
	}
	.product-unit img{
		height: 20vh;
	}

	.product-unit:hover{
		background:rgba(245, 248, 250, 0.44);
	}

</style>
<script  src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {

		$('.star').on('click', function () {
			$(this).toggleClass('star-checked');
		});

		$('.ckbox label').on('click', function () { $(this).parents('tr').toggleClass('selected'); 	});
		$('.btn-filter').on('click', function () {
			var $target = $(this).data('target');
			if ($target != 'all') {
				var products = $('.product-unit').toArray();
				console.log($target);
				products.forEach(function(element) {
						var product = $(element);
						var productData = $(product).data('status');
						var include = productData.includes($target);
						if (include) { $(product).css('display', 'none').fadeIn('slow'); }
						else { $(product).css('display', 'none').fadeOut('slow'); }
				});
			} else { $('.table tr').css('display', 'none').fadeIn('slow'); }









			// original 
					// 		    $('.btn-filter').on('click', function () {
				 //      var $target = $(this).data('target');
				 //      if ($target != 'all') {
				 //        $('.table tr').css('display', 'none');
				 //        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
				 //      } else {
				 //        $('.table tr').css('display', 'none').fadeIn('slow');
				 //      }
				 //    });

				 // });  
	  
		});

	$('.product-unit').on('click', 'button.publishmp-button', function (){
		$(this).toggleClass('btn-success','btn-default');
		var text = $(this).find('span').text();
		$(this).children('span').text(text == "Si" ? "No" : "Si");
	});
	$('.product-unit').on('click', 'button.owsoftware-button', function (){
		$(this).toggleClass('btn-success','btn-default');
		var text = $(this).find('span').text();
		$(this).children('span').text(text == "Si" ? "No" : "Si");
	});
	$('.product-unit').on('click', 'button.published_shop-button', function (){ 
		$(this).toggleClass('btn-success','btn-default');
		var text = $(this).find('span').text();
		$(this).children('span').text(text == "Si" ? "No" : "Si");
	});

	});
</script>