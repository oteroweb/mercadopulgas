<h3 class="text-center">Preguntas</h3>
<div class="questions">
	@if (count($questions) == 0)
	 <div class="no-question"> Producto sin preguntas </div>
@else
	@foreach($questions as $question) 
	<div class="question">{{$question->question}}  <span><b>{{$question->created_at}} </b> </span></div>
		<div class="answer">
		@if ($question->answer == null)
			Sin respuesta
		@else 
			{{$question->answer}}  {{$question->created_at}}
		@endif
		<hr>
		</div>

	@endforeach
@endif

	@if(Auth::guest() or $product->user_id == Auth::user()->id)
		
	@else
<form class="form-horizontal question-form" role="form" method="POST" action="{{ route('question') }}">
        {{ csrf_field() }}
        		@if (session('question'))
    				<div class="alert alert-success">
        				{{ session('question') }}
    				</div>
				@endif
                        <!-- Select: Foto -->
                        <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">

                            <div class="col-md-10 col-md-offset-1">
                                 <textarea rows="4" id="question" type="text" required class="form-control question-input" placeholder="Realiza tu pregunta..." name="question" value="{{ old('question') }}" >
                                 </textarea>
                                @if ($errors->has('question'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('question') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: Foto> -->
                        <!-- Select: Foto -->
                        <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                 <input id="product_id" type="hidden" class="form-control" name="product_id" value="{{ $product->id }}" >
                                @if ($errors->has('product_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('product_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- </Select: Foto> -->

						<div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn-lg btn-primary">
                                    Preguntar
                                </button>

                    
                            </div>
                        </div>
                    </form>
	@endif

</div>
