    @if(Auth::guest())
        <div class="guest-buy">Para comprar debes iniciar sesion </div>
    @else
            @if($product->user_id == Auth::user()->id)
                <div class="own-question">No puedes comprar tus propios articulos</div>
            @else
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#buyModal"> Comprar </button>
                <div class="modal fade" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('validate.purchase',['slug' => $product->slug ]) }}">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="favoritesModalLabel">Comprando</h4>
                                </div>
                                <div class="modal-body">
                                    {{ csrf_field() }}
                                    Advertencia este paso no puede ser desecho, y puede generar cargos
                                    <br> Esta realizando la compra por el articulo
                                    <br> 
                                    Nombre del articulo = {{$product->promotional_title}}
                                    <br> 
                                    Costo Total en tu compra = {{$product->price_sell}} Bs.F
                                    <br>
                                    Foto del Articulo = <img src="{{($product->picture1) ? asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> 
                                    <br>
                                    <br>
                                    Ingrese su contraseña nuevamente para continuar 
                                    <!-- Select: cantidad de producto -->
                                    <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                        <label for="quantity" class="col-md-4 control-label">Cantidad: </label>
                                        <div class="col-md-6">
                                            <input id="quantity" type="number" required class="form-control" name="quantity" value="{{ old('quantity') }}" > Bs.F.
                                            @if ($errors->has('quantity'))
                                                <span class="help-block"> <strong>{{ $errors->first('quantity') }}</strong> </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- </Select: cantidad de producto> -->
                                    <!-- Select: contraseña -->
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Contraseña </label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" required class="form-control" name="password" value="{{ old('password') }}" >
                                            @if ($errors->has('password'))
                                            <span   class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- </Select: contraseña> -->
                                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                        <div class="col-md-6">
                                            <input id="slug" type="hidden"  class="form-control" name="slug" value="{{ $slug }}" >
                                            @if ($errors->has('slug'))
                                            <span   class="help-block">
                                                <strong>{{ $errors->first('slug') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">     
                                    <button type="submit"  class="btn btn-default"     data-dismiss="modal">Cerrar</button>
                                    <span class="pull-right"> <button type="submit" class="btn btn-primary"> Comprar     </button> </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
    @endif
