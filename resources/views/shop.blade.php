@extends('layouts.app') 
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                
                <div class="panel-body">
                    @if( is_null($shop) ) 
                        @foreach ($categories as $category)
                            <a href="{{route('category',['category' => $category->slug]) }} ">{{ $category->name }}</a>
                        @endforeach
                      @else
                        <link class="estilo" href="{{asset('storage/themes/')}}/{{(Auth::user()->template != '') ? Auth::user()->template : ''}}.css" rel="stylesheet">
                        @foreach ($categories as $category)
                            <a href="{{route('shop.category',['category' => $category->slug,'shop'=>$shop]) }} ">{{ $category->name }}</a>
                        @endforeach
                     @endif   
                </div>
                <div> <h1>Productos</h1> 
                  @foreach ($products as $product)
                        <h1> <a href="{{route('product.shop',
                        [ 
                        'category'=>$product->category()->first()->slug,
                        'shop'=>$shop,
                        'product'=>$product->slug
                        ])}} ">{{ $product->promotional_title }}</a> </h1>
                        <h2> {{$product->promotional_subtitle}}</h2>
                        <img src="{{($product->picture1) ? 
                        asset('storage/products/'.$product->picture1) : asset('storage/products/sinfoto.png') }}" alt="{{ $product->promotional_title }}"/> 
                        <p> {{$product->price_sell}} Bs.F. </p>
                    @endforeach
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
