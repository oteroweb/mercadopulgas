 <ul class="categories-list">
  @if( is_null($shop) )
  @foreach ($categories as $category)
  <li>
    <a href="{{route('category',['category' => $category->slug]) }} ">{{ $category->name }}</a>
  </li>
  @endforeach
</ul>
@else
<link class="estilo" href="{{asset('storage/themes/')}}/{{(Auth::user()->template != '') ? Auth::user()->template : ''}}.css" rel="stylesheet">
<ul class="categories-list">
  @foreach ($categories as $category)
  <li>
    <a href="{{route('shop.category',['category' => $category->slug,'shop'=>$shop]) }} ">{{ $category->name }}</a>
  </li>
  @endforeach
</ul>
@endif     