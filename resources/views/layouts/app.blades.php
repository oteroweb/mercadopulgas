<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="no-js"  dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script> 
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

    </script>
        @stack('scripts')
</head>
<body>
<header data-sticky-container>

<div class="title-bar" data-responsive-toggle="responsive-menu" data-hide-for="medium" >
  <button class="menu-icon" type="button" data-toggle="responsive-menu"></button>
  <div class="title-bar-title">Menu</div>
</div>

<div class="top-bar grid-x" id="responsive-menu" data-sticky data-options="marginTop:0;">
   <img src="{{ asset('img/mercadopulgas-logo.png') }}" alt="">
    <div class="cell small-4">
    <ul class="menu">
        <li><input type="search" placeholder="Search"></li>
        <li><button type="button" class="button">Search</button></li>
        </ul>
    </div>
    <div class="cell small-4">the left</div>
    <div class="cell small-4">the left</div>
</div>
  
  

</header>


  
  
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/foundation.min.js') }}"></script>

    <script>
    $(document).foundation();

$('.title-bar').on('sticky.zf.stuckto:top', function(){
  $(this).addClass('shrink');
}).on('sticky.zf.unstuckfrom:top', function(){
  $(this).removeClass('shrink');
});
     </script>

     <style> 
     nav.top-bar {
        text-align:center;
        }
   

</style> 
</body>
</html>
