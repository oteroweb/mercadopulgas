<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from themes.iamabdus.com/bigbag/1.3/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Aug 2017 08:32:32 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>

    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BIGBAG Store - Ecommerce Bootstrap Template</title>

    <!-- PLUGINS CSS STYLE -->
    <link href="{{asset('css/jquery-ui.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/select_option1.css')}}" rel="stylesheet">
    <link href="{{asset('css/revslider/settings.css')}}" rel="stylesheet">
    <link href="{{asset('css/owlcarousel/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('css/shop/style.css')}}" rel="stylesheet">
    
    @if ($options->style == "" ) 
      <link href="{{asset('css/shop/default.css')}}" id="option_color" rel="stylesheet">
    @else 
    <link href="{{asset('css/shop/'.$options->style)}}" id="option_color" rel="stylesheet">


    @endif
    <link href="{{asset('css/shop/optionswitch.css')}}" rel="stylesheet">

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>

    <!-- Icons -->
    <link rel="shortcut icon" href="img/favicon.png">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body class="{{($options->layout == 'boxed') ? 'body-wrapper bodyColor wrapper default' : 'body-wrapper'}} {{($options->background_pattern != '') ? $options->background_pattern : ''}} {{($options->menu == 'fixed') ? '' : 'static'}}">

    <div class="main-wrapper">

    

   @yield('content')
   
    </div>

    <!-- LOGIN MODAL -->
    <div class="modal fade login-modal" id="login" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">INICIAR SESION</h3>
          </div>
          <div class="modal-body">
            <form action="#" method="POST" role="form">
              <div class="form-group">
                <label for="">Ingresar correo</label>
                <input type="email" class="form-control" id="">
              </div>
              <div class="form-group">
                <label for="">Contraseña</label>
                <input type="password" class="form-control" id="">
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Recordarme
                </label>
              </div>
              <button type="submit" class="btn btn-primary btn-block">Iniciar Sesion</button>
              <button type="button" class="btn btn-link btn-block">Olvide mi contraseña?</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- SIGN UP MODAL -->
    <div class="modal fade" id="signup" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">Crear Cuenta</h3>
          </div>
          <div class="modal-body">
            <form action="#" method="POST" role="form">
              <div class="form-group">
                <label for="">Ingresar Correo</label>
                <input type="email" class="form-control" id="">
              </div>
              <div class="form-group">
                <label for="">Contraseña</label>
                <input type="password" class="form-control" id="">
              </div>
              <div class="form-group">
                <label for="">Confirmar Contraseña</label>
                <input type="password" class="form-control" id="">
              </div>
              <button type="submit" class="btn btn-primary btn-block">Registrar</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- PORDUCT QUICK VIEW MODAL -->
    <div class="modal fade quick-view" id="quick" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="media">
              <div class="media-left">
                <img class="media-object" src="{{asset('images/shop/products/quick-view/quick-view-01.jpg')}}" alt="Image">
              </div>
              <div class="media-body">
                <h2>Titulo de Producto</h2>
                <h3>10.000,00 Bs.F.</h3>
                <p>Descripcion Producto.</p>
                <span class="quick-drop">
                  <select name="guiest_id3" id="guiest_id3" class="select-drop">
                    <option value="0">Medida</option>
                    <option value="1">S</option>
                    <option value="2">M</option>
                    <option value="3">L</option>
                  </select>
                </span>
                <span class="quick-drop resizeWidth">
                  <select name="guiest_id4" id="guiest_id4" class="select-drop">
                    <option value="0">Cantidad</option>
                    <option value="1">Cantidad 1</option>
                    <option value="2">Cantidad 2</option>
                    <option value="3">Cantidad 3</option>
                  </select>
                </span>
                <div class="btn-area">
                  <a href="#" class="btn btn-primary btn-block">Añadir Al Carrito <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/revslider/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('js/revslider/jquery.themepunch.revolution.min.js')}}"></script>
    
    <script src="{{asset('js/owlcarousel/owl.carousel.js')}}"></script>
    <script src="{{asset('js/selectbox/jquery.selectbox-0.1.3.min.js')}}"></script>
    <script src="{{asset('js/countdown/jquery.syotimer.js')}}"></script>
    <script src="{{asset('js/shop/custom.js')}}"></script>
    
    @if(Auth::guest() or $shop == Auth::user()->name_shop_slug)
    <script src="{{asset('js/shop/optionswitcher.js')}}"></script>
    @endif


<script></script>
</body>

<!-- Mirrored from themes.iamabdus.com/bigbag/1.3/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Aug 2017 08:34:11 GMT -->
</html>
