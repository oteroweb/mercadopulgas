<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script> 
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

    </script>
        @stack('scripts')
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?49gsUhBrrem76tMAt42pfZuCJij94hOY";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="navbar-header">
                            <!-- Collapsed Hamburger -->
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!-- Branding Image -->
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <!-- {{ config('app.name') }} -->
                                <img src="{{ asset('img/mercadopulgas-logo.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <form class="navbar-header-search">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Busca tu producto">
                                    <span class="input-group-btn ">
                                        <button class="btn btn-primary" type="button">Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            <!-- Left Side Of Navbar -->
                            <ul class="nav navbar-nav">
                                &nbsp;
                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right navbar-header-options">
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                    <li><a href="{{ route('login') }}">Ingresar</a></li>
                                    <li><a href="{{ route('register') }}">Registro</a></li>
                                @else

                                     <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            Mi cuenta <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">

                                            <li> <a href="{{route('dashboard.index' )}} ">  Tablero</a></li>
                                            {{-- 
                                        
                                            <li> <a href="{{route('dashboard.products')}}"> Productos</a></li>
                                            <li> <a href="{{route('dashboard.answers')}}">   Respuestas</a></li>
                                            <li> <a href="{{route('dashboard.products')}}"> Clientes</a></li>
                                            <li> <a href="{{route('dashboard.clients')}} "> Compras</a></li>
                                            <li> <a href="{{route('dashboard.sells')}} ">   Ventas</a></li>
                                            <li> <a href="{{route('dashboard.questions')}}">Preguntas</a></li>
                                            <li> <a href="{{route('dashboard.support')}} "> Soporte</a></li>
                                            --}}
                                        </ul>
                                    </li>




                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                        {{--  
                                            <li> <a href="{{route('sell')}} ">Vender</a></li>
                                            <li> <a href="{{route('shop',['shop'=> Auth::user()->name_shop_slug])}} ">Mi Tienda</a></li>
                                        --}}
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>
    <footer class="footer">
        <div class="footer__text">
            <div class="footer__copyright">Copyright 2017 MercadoPulgas SRL RIF: J-88676545-4</div>
            <ul class="footer__navigation">
                <li> <a href="#">Términos y condiciones</a></li>
                <li> <a href="#">Políticas de privacidad</a></li>
                <li> <a href="#">Ayuda</a></li>
            </ul>
        </div>
            <div class="footer__app_link">
                <img src="{{ asset('img/mercadopulgas-download-app.png') }}">
            </div>
    </footer>
    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/foundation.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

</body>
</html>
