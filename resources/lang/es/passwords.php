<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La Contraseña debe tener por lo menos seis caracteres y debe coincidir con Confirmar Contraseña.',
    'reset' => 'Su contraseña ha sido reiniciada!',
    'sent' => 'Te hemos enviado un correo con tu enlace de reinicio de contraseña',
    'token' => 'Este enlace de reinicio es invalido.',
    'user' => "No podemos encontrar un usuario con ese correo electronico.",

];
