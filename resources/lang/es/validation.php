<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El :attribute debe ser aceptado.',
    'active_url'           => 'El :attribute No es una URL Valida.',
    'after'                => 'El Campo :attribute debe ser una date after :date.',
    'after_or_equal'       => 'El Campo :attribute debe ser una date after or equal to :date.',
    'alpha'                => 'El Campo :attribute puede solo contener letras.',
    'alpha_dash'           => 'El Campo :attribute puede solo contener letras, numeros, y dashes.',
    'alpha_num'            => 'El Campo :attribute puede solo contener letras y numeros.',
    'array'                => 'El Campo :attribute must be an array.',
    'before'               => 'El Campo :attribute debe ser una fecha before :date.',
    'before_or_equal'      => 'El Campo :attribute debe ser una fecha before or equal to :date.',
    'between'              => [
        'numeric' => 'El Campo :attribute debe ser ente :min y :max.',
        'file'    => 'El Campo :attribute debe ser ente :min y :max kilobytes.',
        'string'  => 'El Campo :attribute debe ser ente :min y :max characters.',
        'array'   => 'El Campo :attribute must have between :min y :max items.',
    ],
    'boolean'              => 'El Campo :attribute debe ser Verdadero o Falso.',
    'confirmed'            => 'El Campo :attribute confirmation does not match.',
    'date'                 => 'El Campo :attribute is not a valid date.',
    'date_format'          => 'El Campo :attribute does not match the format :format.',
    'different'            => 'El Campo :attribute y :other must be different.',
    'digits'               => 'El Campo :attribute must be :digits digits.',
    'digits_between'       => 'El Campo :attribute debe ser ente :min y :max digits.',
    'dimensions'           => 'El Campo :attribute has invalid image dimensions.',
    'distinct'             => 'El Campo :attribute field has a duplicate value.',
    'email'                => 'El Campo :attribute must be a valid email address.',
    'exists'               => 'El Campo selected :attribute is invalid.',
    'file'                 => 'El Campo :attribute must be a file.',
    'filled'               => 'El Campo :attribute field must have a value.',
    'image'                => 'El Campo :attribute must be an image.',
    'in'                   => 'El Campo selected :attribute is invalid.',
    'in_array'             => 'El Campo :attribute field does not exist in :other.',
    'integer'              => 'El Campo :attribute must be an integer.',
    'ip'                   => 'El Campo :attribute must be a valid IP address.',
    'json'                 => 'El Campo :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'El Campo :attribute may not be greater than :max.',
        'file'    => 'El Campo :attribute may not be greater than :max kilobytes.',
        'string'  => 'El Campo :attribute may not be greater than :max characters.',
        'array'   => 'El Campo :attribute may not have more than :max items.',
    ],
    'mimes'                => 'El Campo :attribute must be a file of type: :values.',
    'mimetypes'            => 'El Campo :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'El Campo :attribute must be at least :min.',
        'file'    => 'El Campo :attribute must be at least :min kilobytes.',
        'string'  => 'El Campo :attribute must be at least :min characters.',
        'array'   => 'El Campo :attribute must have at least :min items.',
    ],
    'not_in'               => 'El Campo selected :attribute is invalid.',
    'numeric'              => 'El campo :attribute debe ser un numero.',
    'present'              => 'El Campo :attribute field must be present.',
    'regex'                => 'El Campo :attribute format is invalid.',
    'required'             => 'el campo :attribute es requerido.',
    'required_if'          => 'El Campo :attribute field is required when :other is :value.',
    'required_unless'      => 'El Campo :attribute field is required unless :other is in :values.',
    'required_with'        => 'El Campo :attribute field is required when :values is present.',
    'required_with_all'    => 'El Campo :attribute field is required when :values is present.',
    'required_without'     => 'El Campo :attribute field is required when :values is not present.',
    'required_without_all' => 'El Campo :attribute field is required when none of :values are present.',
    'same'                 => 'El Campo :attribute y :other must match.',
    'size'                 => [
        'numeric' => 'El Campo :attribute must be :size.',
        'file'    => 'El Campo :attribute must be :size kilobytes.',
        'string'  => 'El Campo :attribute must be :size characters.',
        'array'   => 'El Campo :attribute must contain :size items.',
    ],
    'string'               => 'El :attribute debe ser una cadena de texto.',
    'timezone'             => 'El Campo :attribute must be a valid zone.',
    'unique'               => 'El :attribute  ingresado, ya ha sido tomado, por favor intente con otro.',
    'uploaded'             => 'el campo :attribute fallo al subirse por favor intente nuevamente.',
    'url'                  => 'El Campo :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
    'promotional_title'=> "Titulo Promocional",
    'promotional_subtitle'=> "Subtitulo Promocional",
    'price_sell'=> "Precio de Venta",
    'picture1' =>'Foto 1',
    'name' =>'Nombre',
    'description' =>'Descripción',
    'question' => 'Pregunta',  
    'name_shop' => 'Nombre de tu Tienda',
    'password' => 'Contraseña',
    'confirmation' => 'Confirmacion',
    'password_confirmation' => 'Confirmar Contraseña',
    'phone' => 'Telefono'


    ],

];
