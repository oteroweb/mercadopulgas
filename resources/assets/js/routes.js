import Q0 from './components/questions/q0.vue';
import Q1 from './components/questions/q1.vue';
import Q2 from './components/questions/q2.vue';
import Q3 from './components/questions/q3.vue';
import Q4 from './components/questions/q4.vue';
import Q5 from './components/questions/q5.vue';
import Q6 from './components/questions/q6.vue';
import Q7 from './components/questions/q7.vue';
export const routes = [
    {path: '/', component: Q0},
    {path: '/q1', component: Q1},
    {path: '/q2', component: Q2},
    {path: '/q3', component: Q3},
    {path: '/q4', component: Q4},
    {path: '/q5', component: Q5},
    {path: '/q6', component: Q6},
    {path: '/q7', component: Q7},
];