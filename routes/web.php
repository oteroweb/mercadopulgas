<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

        Route::get('/', 'HomeController@index')->name('welcome');
        Route::get('/tienda/{shop}', 'HomeController@myshop')->name('shop');
        Route::get('/categoria/{category}', 'HomeController@category')->name('category');
        Route::get('/tienda/{shop}/{category}', 'HomeController@category')->name('shop.category');
        Route::get('categoria/{category}/{product}', 'HomeController@product')->name('product');
        Route::get('/tienda/{shop}/{category}/{product}', 'HomeController@product')->name('product.shop');

        Route::group(['middleware' => 'auth'], function () {
          Route::get('vender','HomeController@sell')->name('sell');
          Route::post('vender','HomeController@sellPublish')->name('sell.publish');
          Route::post('/pregunta','HomeController@question')->name('question');
          Route::post('/compra','HomeController@purchase')->name('purchase');
          Route::post('/validarCompra/','HomeController@validatePurchase')->name('validate.purchase');
          Route::group(['prefix' => 'micuenta'], function () {
            Route::get('/', 'ProfileController@index')->name('dashboard.index');
            Route::get('/productos', 'ProfileController@products')->name('dashboard.products');
            Route::get('/compras', 'ProfileController@purchases')->name('dashboard.purchases');
            Route::get('/ventas', 'ProfileController@sells')->name('dashboard.sells');
            
            Route::get('/soporte', 'ProfileController@settings')->name('dashboard.settings');
            Route::get('/clientes', 'ProfileController@clients')->name('dashboard.clients');
            Route::get('/vendedores', 'ProfileController@sellers')->name('dashboard.sellers');
            Route::get('/preguntas', 'ProfileController@questions')->name('dashboard.questions');
            Route::get('/respuestas', 'ProfileController@answers')->name('dashboard.answers');
            Route::get('/soporte', 'ProfileController@support')->name('dashboard.support');
            // 
            // falla imporante resolver problema de que puede editar productos de otros usuarios añadir middlewre
            // Route::group(['middleware' => 'ownproduct'], function () {
            // Route::group([], function () {
                Route::get('/productos/editar/{slug}', 'Dashboard\ProductController@edit')->name('dashboard.products.edit');
                Route::post('/productos/editar', 'Dashboard\ProductController@update')->name('dashboard.products.update');
            // });
          });
        });



        // Authentication Routes...
        $this->get('ingreso', 'Auth\LoginController@showLoginForm')->name('login');
        $this->post('ingreso', 'Auth\LoginController@login');
        $this->post('cerrar', 'Auth\LoginController@logout')->name('logout');

        // Registration Routes...
        $this->get('registro', 'Auth\RegisterController@showRegistrationForm')->name('register');
        $this->post('registro', 'Auth\RegisterController@register');

        // Password Reset Routes...
        $this->get('clave/reinicio', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        $this->post('clave/reinicio', 'Auth\ResetPasswordController@reset');
        $this->get('clave/reinicio/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        $this->post('clave/correo', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        // Auth::routes(); //en vez de esto

