<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categoriesprincipal','Api\CategoryController@getCategoriesPrincipal');
Route::get('consulta/cedula/{nationality}/{cedula}','HomeController@cedula')->name('consulta.cedula');
Route::get('consulta/rif/{nationality}/{rif}','HomeController@rif')->name('consulta.rif');

Route::get('getThemes','HomeController@getThemes')->name('getThemes');


Route::group([
    'middleware' => ['cors', 'api'],
], function () {
    Route::post('/auth/token/issue', 'AuthController@issueToken');
    Route::post('/auth/token/refresh', 'AuthController@refreshToken');

    Route::group([
        'middleware' => 'jwt.auth',
    ], function () {
        Route::post('/auth/token/revoke', 'AuthController@revokeToken');
        // Route::get('/categories/full-list', 'CategoriesController@fullList');

        // Route::resource('/categories', 'CategoriesController', [
        //     'except' => ['create', 'edit'],
        // ]);

        // Route::resource('/products', 'ProductsController', [
        //     'except' => ['create', 'edit'],
        // ]);

        Route::get('/me', 'MeController@show');
    });
});