<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $name= $faker->name;
    return [
        'name' => $name,
        'name_shop' => $name."shop",
        'name_shop_slug' => str_slug($name." shop", "_"),
        'address_home' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('password'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    $promo_title = $faker->name;
    return [
        'code' => $faker->name,
        'promotional_title' => $promo_title,
        'slug' => str_slug($promo_title,'_'),
        'promotional_subtitle' => "promotional_subtitle ".$promo_title,
        'promotional_title' => "promotional_title ".$promo_title,
        'picture1' => null,
        'picture2' => "http://lorempixel.com/640/480/?24098):",
        'description' => "description ".$promo_title,
        'price_sell' => 9.99,
        'disponibility' => 5,
        'is_new' => rand(0,1),
        'publish' => rand(0,1),
        'publishmp' => rand(0,1),
        'category_id' => rand(0,30),
    ];

    
});