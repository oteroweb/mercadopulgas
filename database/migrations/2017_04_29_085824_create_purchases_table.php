<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity')->default(0);
            
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');

            $table->integer('seller_id')->unsigned();
            $table->string('seller_comment')->nullable();
            $table->double('seller_rate')->nullable()->default(-1);
            $table->foreign('seller_id')->references('id')->on('users');

            $table->integer('buyer_id')->unsigned();
            $table->double('buyer_rate')->nullable()->default(-1);
            $table->string('buyer_comment')->nullable();
            $table->foreign('buyer_id')->references('id')->on('users');

             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('purchases');
    }
}
