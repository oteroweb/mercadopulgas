<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
             $table->increments('id');
             $table->string('code')->default(0); //codigo de facturacion
             $table->string('name')->default("nombre"); //nombre ante el inventario
             $table->string('name_short')->default("nombre corto"); //nombre corto para reconocimiento rapido
             $table->string('mark')->default("sin marca")->nullable(); //marca 
             $table->string('model')->default("sin modelo")->nullable(); //modelo de la marca
             $table->string('deparment')->default("departamento")->nullable(); //departamento asociado a la tienda
             $table->string('reference')->default("sin referencia")->nullable(); //referencia para ubicarlo rapidamente
             $table->string('days_guarante')->default("dias garantia")->nullable(); //fecha de vencimiento
            $table->string('promotional_type')->default("Promocion por defecto")->nullable(); // stock
            $table->boolean('is_new')->default(true);	
            $table->integer('sales')->default(0); // ventas
        // configuracion
             $table->integer('unit')->default(0); //unidad en la que se expresa el producto
             $table->double('fee_buy',15,8)->default(0.00); //iva con el que se compra el producto
             $table->double('fee_sell',15,8)->default(0.00); //iva con el que se vendera el producto
             $table->double('price_sell',15,8)->default(0.00); //precio con el se vendera va determinado por costos
             $table->integer('decimal_expresed')->default(2); // cantidad de decimales en los que se expresara
        // costos //seccion de costos para el producto
            $table->double('cost_method',15,8)->default(0.00); // select para los tipos de costos establecidos
            $table->double('cost_provider',15,8)->default(0.00); // Costo directo del proveedor
            $table->double('cost_calculate',15,8)->default(0.00); // costo calculado
            $table->double('cost_last',15,8)->default(0.00); // ultimo costo
            $table->double('cost_average',15,8)->default(0.00); // costo promedio
            $table->double('price_offer',15,8)->default(0.00); // costo precio oferta
            $table->double('price_max',15,8)->default(0.00); // precio maximo
            $table->double('price_min',15,8)->default(0.00); // precio minomo
            $table->double('price_mayor',15,8)->default(0.00); // precio mayor
            $table->string('promotional_title')->default("Titulo Articulo"); // titulo promocional
            $table->string('promotional_subtitle')->default("Subtitulo Articulo"); // Subtitulo
            $table->string('picture1')->default(null)->nullable();
            $table->integer('category_id')->default(0)->unsigned();
            $table->integer('user_id')->default(0)->unsigned();
            $table->string('slug')->unique();
             // costo promedio
            $table->string('picture2')->default(null)->nullable();// costo promedio
            $table->integer('disponibility')->nullable(); // stock

            $table->string('description')->nullable(); // descripcion del articulo
            $table->string('guarantee')->nullable(); // garantia
            $table->timestamps();
             $table->softDeletes();



            $table->double('other_cost',15,8)->nullable(); // total de otros costos
            $table->double('cost_ship',15,8)->nullable(); // total de otros costos
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');            


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        // $table->dropForeign('products_user_id_foreign');
        // $table->dropForeign('products_category_id_foreign');
        Schema::dropIfExists('products');
    }
}
