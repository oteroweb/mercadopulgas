<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // codig
        // tipo de cliente natural.....down1re nombre
        // segundo nombre
        // primer apellido 
        // segundo apellido
        // denominacion fiscal
        // pais
        // referencia

        
        Schema::defaultStringLength(191);
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->default("s/n");
            $table->enum('fiscal',['contribuyente','no contribuyente']);
            $table->string('name');
            $table->boolean('is_buy')->default(0);
            $table->boolean('is_sell')->default(0);
            $table->boolean('is_shop')->default(0);
            $table->boolean('is_ticket')->default(0);
            $table->string('name_shop')->nullable();
            $table->string('name_shop_slug')->unique()->nullable();
            $table->string('document')->default(0);


            $table->string('template')->default(null)->nullable();
            $table->string('reference')->default(0);
            $table->string('address_home')->default(0);
            $table->string('address_work')->default(0);
            $table->string('state')->default('Caracas');
            $table->string('country')->default("Distrito Capital");
            $table->string('city')->default('caracas');
            $table->string('phone')->default('02510000000');
            $table->string('cellphone')->default('04160000000');
            $table->string('fax')->default('02510000000');;

            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
