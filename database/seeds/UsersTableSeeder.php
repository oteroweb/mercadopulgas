<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
         DB::table('users')->insert([
            'name' => 'oteroweb',
            'email' => "oterolopez1990".'@gmail.com',
            'password' => bcrypt('saratoga1990'),
            'name_shop' => 'oteroweb_shop',
            'name_shop_slug' => 'oteroweb_shop',
        ]);
            DB::table('users')->insert([
            'name' => 'raicoacosta',
            'email' => "me@raicoacosta.com",
            'password' => bcrypt('raicoacosta'),
            'name_shop' => 'raicoacosta_shop',
            'name_shop_slug' => 'raicoacosta_shop',
        ]);
        factory(App\User::class, 1)->create()->each(function ($u) {
         $u->products()->save(factory(App\Product::class)->make());
        });
    }
}
