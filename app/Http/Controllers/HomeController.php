<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Validator;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *home
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories= \App\Category::where('parent',0)->get();
        
        return view('welcome',['categories' => $categories]);
    }

    public function myshop($shop = null)
    {   

        if(!$shop)  { $products = \App\Product::all();}
        else {
            $myshop = \App\User::where('name_shop_slug',$shop)->firstOrFail();
            $products = $myshop->products()->select(['id','promotional_title','promotional_subtitle','price_sell','picture1','category_id','slug'])->get();
            // foreach ($products->all() as $product){
            //     $category = $product->category;       
            //     echo "<pre>";print_r($product->toArray());echo "</pre>";
            //     // $allProduct = $product;       
            //     // $productWCategory= $product->toArray();
            //     // $productWCategory['category']= $category;
            //     // dd($productWCategory);
            //     // echo "<pre>";var_dump($productWCategory);echo "</pre>";

            // }
        }
        $template = (Auth::user()->template != '') ? Auth::user()->template : '';
        $categories= \App\Category::where('parent',0)->get();
        return view('shop', compact('categories','shop','products','template'));

            // ['categories' => $categories,'shop' => $shop,'products'=> $products]);
    }

    public function category(Request $request, $category,$shop = null)
    {

        if(is_null($shop)){
        $products= \App\Category::Select('id')
                    ->where('slug',$category)
                    ->firstOrFail()
                    ->products()
                    ->get([
                        'id',
                        'promotional_title',
                        'promotional_subtitle',
                        'price_sell',
                        'is_new',
                        'picture1',
                        'slug',
                        'category_id',

                    ])
                    ;
        }
        else {
            $shop = $request->route('shop');
            $category = $request->route('category');
            $user_id = \App\User::select('id')->where('name_shop_slug',$shop)->firstOrFail()->id;
            $category_id= \App\Category::Select('id')
                    ->where('slug',$category)
                    ->firstOrFail();
            $products= $category_id->products()->where('user_id',$user_id)->get();
        }
// logica por borrar no la use a la final
        // $productWithCategorySlug= array();
        // foreach ($products as $key => $value) {
        //   // $productWithCategorySlug[] = [$value[],$value->category()->get(['slug'])->slug];
        //   // $productWithCategorySlug = $value->category()->get(['slug'])[0]->slug;
        //   $slug['category_slug'] = $value->category()->get(['slug'])[0]->slug;
        //   $productWithCategorySlug = [$value,$slug];
        //   // $productWithCategorySlug = [$value,;
        //   // dd($productWithCategorySlug);
        // }
        $categories= \App\Category::where('parent',0)->get();
        return view('category', ['products' => $products,'categories'=>$categories, 'shop' =>$shop,'current_category'=>$category]);
    }
    public function product(Request $request, $category,$product, $shop=null){
        


        $slug_product = $request->route('product');
        $shop = $request->route('shop');
        $category = $request->route('category');

      
       if(is_null($shop)) $product = \App\Product::select([
           'promotional_title',
           'promotional_subtitle',
           'id',
           'price_sell',
           'guarantee',
           'picture1',
           'picture2',
           'description','category_id','user_id'])
           ->where('slug',$slug_product)
           ->FirstOrFail();
       else {
           $product =  \App\Product::select([
           'promotional_title',
           'promotional_subtitle',
           'id',
           'price_sell',
           'guarantee',
           'picture1',
           'picture2',
           'description', 'category_id','user_id'])
           ->where('slug',$slug_product)
           ->FirstOrFail();
        }
        $questions = $product->questions()->get();
        $categories= \App\Category::select(['name','slug'])->where('parent',0)->get();
        // $questions = $
       $current_category = $product->category()->FirstOrFail(['name','slug']);
       return view('product',['product'=>$product,'current_category'=>$current_category,'shop'=>$shop,'categories'=>$categories, 'questions' => $questions, 'slug' => $slug_product]);
    }

    public function sell(){

     // mercadolibre pregunta el titulo primeramente y de manera inteligente el automaticamente detecta la categorai en este caso tendriamos que usar un vaciado de categorias 
      $categories = \App\Category::select(['name','id'])->where('parent',0)->get();
      return view('sell',compact('categories'));
    }

    public function sellPublish(Request $request) {
        $validator = Validator::make( $request->all(), [
            'promotional_title' =>  'required|string',
            'promotional_subtitle' =>  'required|string',
            'picture1' =>  'max:2000|mimes:jpeg,bmp,png,gif,jpg',
            'price_sell' =>  'required|numeric',
            'disponibility' =>  'required|numeric',
            'name' =>  'required',
            'description' =>  'required',
            'is_new' =>  'integer',
            'publishmp' =>  'integer',
            'published_shop' =>  'integer',
            'owsoftware'=> 'integer'
        ],

        [ 
        'picture1.image' => 'La foto 1 debe ser en formato de imagen',
        'picture1.max' => 'Foto 1 excede el tamaño permitido',
        'picture1.mimes' => 'Foto 1 debe ser un archivo de imagen',
        'uploaded'=> 'Error al subir el archivo'
        ]

        );
        if ($validator->fails()) {
            return redirect(route('sell'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $file = $request->file('picture1');
        if(!is_null($file)) {
        $updated = Storage::disk('products')->put('', $file);
        } else { $updated = null;}
        $product_new = \App\Product::create([
            'code' => rand(1,1000),
            'category_id' => $request['category'],
            'promotional_title' => $request['promotional_title'],
            'slug' => str_slug(rand(1,1000)." ".$request['promotional_title'], '_'),
            'promotional_subtitle' => $request['promotional_subtitle'],
            'name' => $request['name'],
            'price_sell' => $request['price_sell'],
            'description' => $request['description'],
            'disponibility' => $request['disponibility'],
            'is_new' => isset($request['is_new']) ? true : false,
            'publishmp' => isset($request['publishmp']) ? true : false,
            'publish' => isset($request['publish']) ? true : false,
            'published_shop' => isset($request['published_shop']) ? true : false,
            'owsoftware' => isset($request['owsoftware']) ? true : false,

            'picture1' =>  $updated,
            'user_id' => Auth::user()->id,

        ]);
        $new_product_category = $product_new->category()->get(['slug'])->toArray()[0]['slug'];
        return redirect(route('product',['shop'=>Auth::user()->name_shop_slug, 'product'=> $product_new->slug,'category'=>$new_product_category]));
    } 
    public function question(Request $request){
        $validator = Validator::make( $request->all(), [
            'question' =>  'required',
            'product_id' =>  'required',
        ], []);
        if ($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();
        }
        $question_new = \App\Question::create([
            'question' => $request['question'],
            'user_id' => Auth::user()->id,
            'product_id'=> $request['product_id'],
        ]);
      return back()->with('question', 'Pregunta Realizada Satisfactoriamente');
    }

    public function validatePurchase(Request $request)
        {
         if(Auth::attempt(['email' => Auth::user()->email, 'password' => $request->password])){

           $product_slug = $request->slug;
           $product = \App\Product::select('name', 'id','user_id','picture1','price_sell','slug')->where('slug',$product_slug)->firstOrFail();
           $quantity = intval($request->quantity);
           $buyer_id = Auth::user()->id;

           $new_purchase = \App\Purchase::create([
            'seller_id' => intval($product->user_id),
            'buyer_id' => $buyer_id,
            'product_id' => $product->id,
            'quantity' => $quantity,
            ]);

           return redirect('dashboard.buys')->with('status', 'Compra Realizada!');

         }
         else {
          return back()->withErrors(['errors' => 'contraseña invalida'])->withInput();
        }
      }

      public function cedula ($nationality = "V", $cedula ="19708951") 
      {
   //$rif="J310029539";
   // $url="http://contribuyente.seniat.gob.ve/BuscaRif/BuscaRif.jsp?p_rif=$rif";
    //$ch = curl_init();
    // curl_setopt ($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // almacene en una variable
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $xxx1 = curl_exec($ch);
    // curl_close($ch);
    // // Separamos el resultado en un arreglo y dividirlo por \n\r\n
    // $xxx = explode("\n\r\n", $xxx1);
    // // Con este comando podemos ver toda la pantalla de seniat impresa por reglones de arreglos
    // // print_r($xxx);
    // // Impreme el rif y la razon social
    // print_r($xxx1);

    // exit;
        $url = "http://www.cne.gob.ve/web/registro_electoral/ce.php?nacionalidad=".$nationality."&cedula=".$cedula;
        // $url = "http://www.cne.gob.ve/web/registro_electoral/ce.php?nacionalidad=V&cedula=19708951;
        // www.cne.gob.ve/web/registro_civil/buscar_rep.php?nac=v&ced=19708951
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER,'http://www.cne.gob.ve/');
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0');
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($ch,CURLOPT_FRESH_CONNECT,TRUE);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
        curl_setopt($ch,CURLOPT_TIMEOUT,10);
        $html=curl_exec($ch);
        if($html==false){
            $m=curl_error(($ch));
            error_log($m);
            curl_close($ch);
            $j['error'] = true;
            $j['descripcion'] = $m;
            print json_encode($j);
            return;
        } else {
            curl_close($ch);
            if (strpos($html, '<b>DATOS DEL ELECTOR</b>') > 0) {
                $modo = 1; # Puede Votar
            } else if (strpos($html, '<strong>DATOS PERSONALES</strong>') > 0) {
                $modo = 2; # No Puede Votar
            } else {
                $modo = -1;
                $j['error'] = true;
                $j["descripcion"] = "El usuario no se encuentra inscrito en el registro electoral";
                return json_encode($j);
            }
            $j['error'] = false;
            $j["descripcion"] = "/cne/elector";
            $j['modo'] = $modo;
            // Datos para un elector que puede votar
            if ($j['modo'] == 1) {
                #Obtener Cédula
                $npos = strpos($html, 'align="left">', strpos($html, 'dula:')) + 13;
                $j['cedula'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Nombre
                $npos = strpos($html, 'align="left"><b>', strpos($html, 'Nombre:')) + 16;
                $j['nombre'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Estado
                $npos = strpos($html, 'align="left">', strpos($html, 'Estado:')) + 13;
                $j['estado'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Municipio
                $npos = strpos($html, 'align="left">', strpos($html, 'Municipio:')) + 13;
                $j['municipio'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Parroquia
                $npos = strpos($html, 'align="left">', strpos($html, 'Parroquia:')) + 13;
                $j['parroquia'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Centro
                $npos = strpos($html, '"#0000FF">', strpos($html, 'Centro:')) + 10;
                $j['centro'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Dirección
                $npos = strpos($html, '"#0000FF">', strpos($html, 'Direcci')) + 10;
                $j['direccion'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                $j['servicio'] = 'no';
                #Obtener servicio
                $npos = strpos($html, 'color="#', strpos($html, 'SERVICIO ELECTORAL')) + 16;
                $j['servicio'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
            }
            // Datos para un elector con objeción
            else if ($j['modo'] == 2) {
                // Cambiado Por oteroweb para que diera error si estaba muerto
                $j['error'] = true;
                #Obtener Cédula
                $npos = strpos($html, 'strong> ', strpos($html, 'dula:')) + 8;
                $j['cedula'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Nombre
                $npos = strpos($html, 'strong> ', strpos($html, 'Primer Nombre:')) + 8;
                $j['nombre'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Nombre
                $npos = strpos($html, 'strong> ', strpos($html, 'Segundo Nombre:')) + 8;
                $j['nombre'] .= " " . trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Apellido
                $npos = strpos($html, 'strong> ', strpos($html, 'Primer Apellido:')) + 8;
                $j['nombre'] .= " " . trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Apellido
                $npos = strpos($html, 'strong> ', strpos($html, 'Segundo Apellido:')) + 8;
                $j['nombre'] .= " " . trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Estatus
                $npos = strpos($html, '<td>', strpos($html, 'ESTATUS')) + 4;
                $j['estatus'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Objecion
                $npos = strpos($html, 'strong> ', strpos($html, '>Objeci')) + 8;
                $j['objecion'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Descripción
                $npos = strpos($html, 'strong> ', strpos($html, '>Descripci')) + 8;
                $j['descripcionobjecion'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener institución
                $npos = strpos($html, 'strong> ', strpos($html, 'solventar la objeci')) + 8;
                $j['institucion'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
                #Obtener Requisitos
                $npos = strpos($html, '<td>', strpos($html, 'Requisitos')) + 4;
                $j['requisitos'] = trim(substr($html, ($npos), (strpos($html, '</td>', ($npos)) - ($npos))));
            }
            return json_encode($j);
            // return var_dump(json_encode($j));
        }
    }
        function rif($nationality = "V", $rif ="197089519"){
        // Pentiente Sugerencia de Turecoi
        // $url = "http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=".$nationality.$rif;
        // http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=V197089519 ;
        // http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=v197089519
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER,'http://seniat.gob.ve');
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0');
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($ch,CURLOPT_FRESH_CONNECT,TRUE);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
        curl_setopt($ch,CURLOPT_TIMEOUT,30);
        $html=curl_exec($ch);
        if($html==false){
            $m=curl_error(($ch));
            error_log($m);
            curl_close($ch);
            $j['error'] = true;
            $j['descripcion'] = $m;
            print json_encode($j);
            return;
        } else {
            curl_close($ch);
            if (strpos($html, 'rif:numeroRif="') == false) {
                $j['rif'] = $rif;
                $j['error'] = "El RIF $rif no est&aacute; registrado o no existe";
                return json_encode($j);
            }
            $j['error'] = false;
            $j['descripcion'] = "obtenerContribuyente";
            #Obtener RIF
            $npos = strpos($html, 'rif:numeroRif="') + 15;
            $j['rif'] = trim(substr($html, ($npos), (strpos($html, '"', ($npos)) - ($npos))));
            #Obtener Nombre
            $npos = strpos($html, '<rif:Nombre>') + 12;
            $j['nombre'] = utf8_decode(trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos)))));
            #Obtener Agente de retención
            $npos = strpos($html, '<rif:AgenteRetencionIVA>') + 24;
            $j['retencion'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
            #Obtener Contribuyente
            $npos = strpos($html, '<rif:ContribuyenteIVA>') + 22;
            $j['contribuyente'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
            #Obtener Tasa
            $npos = strpos($html, '<rif:Tasa>') + 10;
            $j['tasa'] = trim(substr($html, ($npos), (strpos($html, '<', ($npos)) - ($npos))));
            return json_encode($j);
        }
    }


    public function getThemes(){
        $directory = "public/";
        $themes = \Storage::files("public/themes");
        $basename = array();
        foreach ($themes as $theme) { 
          $path_part=pathinfo($theme);
          $basename[]=$path_part['filename'];
          // var_dump($path_part);
        }
        // var_dump($basename);
        return json_encode($basename);

    }
}
