<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
// use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $slug)
    {
        $product = \App\Product::where('slug',$slug)->firstOrFail();
        $categories = \App\Category::select(['name','id'])->where('parent',0)->get();
        return view('dashboard.products.edit', compact('product','categories') );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   $data = $request->all();
        $validator = Validator::make( $data, [
            'promotional_title' =>  'required|string',
            'promotional_subtitle' =>  'required|string',
            'picture1' =>  'max:2000|mimes:jpeg,bmp,png,gif,jpg',
            'price_sell' =>  'required|numeric',
            'disponibility' =>  'required|numeric',
            'name' =>  'required', 
            'description' =>  'required',
            'is_new' =>  'integer', 'publishmp' =>  'integer', 'published_shop' =>  'integer',
            'owsoftware' => 'integer'
        ],

        [  'picture1.image' => 'La foto 1 debe ser en formato de imagen',
            'picture1.max' => 'Foto 1 excede el tamaño permitido',
            'picture1.mimes' => 'Foto 1 debe ser un archivo de imagen',
            'uploaded'=> 'Error al subir el archivo' ]

        );
        if ($validator->fails()) {
            return redirect(route('dashboard.products.edit'))
                        ->withErrors($validator)
                        ->withInput();
        }
        if( $request->hasFile('picture1') && $request->file('picture1')->isValid())  {

            $file = $request->file('picture1');
            if(!is_null($file)) {
                $filename = $data['picture1']->hashName();
                $data['picture1'] = $filename;
                $updated = \Illuminate\Support\Facades\Storage::disk('products')->put('', $file);
            } else { $updated = null;}
        }

        (!isset($data['is_new'])) ? $data['is_new'] = 0 : '' ; 
        (!isset($data['publishmp'])) ? $data['publishmp'] = 0 : '' ; 
        (!isset($data['published_shop'])) ? $data['published_shop'] = 0 : '' ; 
        (!isset($data['owsoftware'])) ? $data['owsoftware'] = 0 : '' ; 
        \App\Product::find($request->id)->update($data);
        return redirect(route('dashboard.products') )->with('status', 'Producto Actualizado Satisfactoriamente!');
     //    return redirect()->route('itemCRUD.index')
     //                    ->with('success','Item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
