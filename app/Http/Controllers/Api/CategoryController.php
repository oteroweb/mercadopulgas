<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function getCategoriesPrincipal(){
    	$categories = \App\Category::select(['name','slug'])->where('parent',0)->get();
    	return json_encode($categories);
    }
}
