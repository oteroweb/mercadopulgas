<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ShopController extends Controller
{
	public function index($shop = null) {
		$user = \App\User::where('name_shop_slug', $shop)->first();
		// dd($user);
		// condicional si la tienda no existe
		if($shop == null){
			abort(404);
		}
		// si existe pueden ser dos posibilidades
		else{ 
			
			// si el usuario es invitado condicionales
			if ( \Auth::guest()){ 
				if($user->options == null ) {
				return "esta tienda presenta un problema";
				}
			}
			else {
				if($user->options == null and $shop == \Auth::user()->name_shop_slug ) {
					return redirect()->route('dashboard.template',['shop'=>\Auth::user()->name_shop_slug,]);
				}
				// puede que la tienda no este configurada
				if($user->options == null ) {
					return "esta tienda presenta un problema";
				}
			
			}

			 $options = json_decode($user->options);
			return view('shop.template1.index',['options' =>$options,'shop' => $shop]);
			
			
		
			
		}

	}
  public function getTemplate() {
		return view('shop.getTemplate');
  }
	public function setTemplate($theme){
		$user = \Auth::user();

		$options = [
			"logo_image" => "",
			"layout" => "boxed",
			"background_pattern" => "pattern-01",
			"menu" => "fixed",
			"social_network" => [
				"facebook" => "pagina",
				"twitter" => "pagina",
				"instagram" => "pagina",
				"youtube" => "pagina",
			],
			"sitename" => $user->name_shop,
			"style" => "color-option2.css",
			"header" => [
				[ "image" => "images/shop/home/banner-slider/shoe1.png",],
				[ "image" => "images/shop/home/banner-slider/shoe3.png",],
				[ "image" => "images/shop/home/banner-slider/shoe2.png",],
			],
			"color" => "red",
		];
		$options_json = json_encode($options);
		\Auth::user()->update(['template' => $theme,'options'=> $options_json]);

		session()->forget('msg');
		return redirect()->route('shop.index',['shop'=>\Auth::user()->name_shop_slug]);
	}

	public function setOption($optionName, $optionValue){
	// echo	\Response::json($optionName, $optionValue);
		echo ($optionName.$optionValue);

	}
	
}
