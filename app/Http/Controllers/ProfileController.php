<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
     	$this->middleware('auth');
     }

     public function index() {
     	return view("dashboard.index");
     }

     public function products() {
          $products = \App\Product::select(['id','promotional_title','promotional_subtitle','price_sell','picture1','disponibility','publishmp','published_shop','owsoftware','slug','category_id','user_id'])->where('user_id',\Auth::user()->id)->get();
     	return view("dashboard.products",compact('products'));
     }

     public function purchases() {
     	$purchases = \App\Purchase::select(['id','quantity','product_id','seller_id','buyer_id','created_at'])->where('buyer_id', \Auth::user()->id)->get();
          return view("dashboard.purchases",compact('purchases'));
     }

     public function sells() {
          $sells = \App\Purchase::select(['id','quantity','product_id','seller_id','buyer_id','created_at'] )->where('seller_id', \Auth::user()->id) ->get();
          return view("dashboard.sells", compact('sells') );
     }



     public function support() {
     	return view("dashboard.support");
     }


     public function questions() {
          return view("dashboard.questions");
     }
     public function answers() {
          return view("dashboard.answers");
     }



     public function settings() {
     	return view("dashboard.settings");
     }
     public function clients(){
     	return view("dashboard.clients");
     }
     public function sellers(){
     	return view("dashboard.sellers");
     }
}
