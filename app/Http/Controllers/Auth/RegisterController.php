<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/tienda/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


        protected function redirectTo()
    {
        return '/tienda/'.Auth::user()->name_shop_slug;
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data);
        return Validator::make($data, [
        //  'code','fiscal',
        // 'document','reference','adress_home','adress_work',
        // 'state','country','city','phone','cellphone','fax',
            // 'is_buy'=> 'string',
            // 'is_shop'=> 'string',
            // 'is_ticket'=> 'string',
            // 'is_sell'=> 'string',
            'document'=>'string|required|min:6|unique:users',
            'name_shop'=>'max:255|string|required|unique:users',
            'address_home' => 'max:255|required|string',
            'phone'=>'digits:11|required|numeric',
            'template'=>'string|required',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            // 'name_shop_slug' => 'max:255|unique:users',
            'password' => 'required|min:6|confirmed',

        ],$messages = [
      'required' => 'El campo :attribute es Requerido.',
      'max'=>'El campo :attribute tiene demasiados Caracteres solo son permitidos 255',
      'numeric'=>'El campo :attribute solo permite numeros',
            'address_home.required' => "El campo Dirección es requerido",
    'phone.digits' => 'Tu Numero Telefonico debe tener El siguiente Formato 04160000000',
    'password.confirmed'=>'Las contraseñas no coindicen'
    ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {  
        return User::create([
            'name' => $data['name'],
            'name_shop' => $data['name_shop'],
            'name_shop_slug' => str_slug($data['name_shop'], '_'),
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
             'is_buy'=> isset($data['is_buy']) ? true : false,
            'is_shop'=> isset($data['is_shop']) ? true : false,
            'is_ticket'=> isset($data['is_ticket']) ? true : false,
            'is_sell'=> isset($data['is_sell']) ? true : false,
            'address_home'=>$data['address_home'],
            'phone'=>$data['phone'],
            'template'=>$data['template'],
            'document' => $data['document']
            

        ]);
        // dd( $new_user);
        // return Redirect(route('shop'),['shop' => $new_user->name_shop_slug]);
        // return redirect()->route('shop', ['shop' => $new_user->name_shop_slug]);

    }
        public function showRegistrationForm()
    {   

        return view('auth.register');
    }
}
