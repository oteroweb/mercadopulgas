<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/tablero/';
   protected function redirectTo()
    {
        if (Auth::user()->typeUser == 1){return '/tablero/shop';}
        else { return '/tablero'; }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
      $messages = [
      'required' => 'El campo :attribute es Requerido.',
      'email.email' => 'El campo Correo Electronico debe contener un correo valido.',
      'typeUser.required' => 'El tipo de persona es requerido',
      'max'=>'El campo :attribute tiene demasiados Caracteres solo son permitidos 255',
      'numeric'=>'El campo :attribute solo permite numeros',
      'address_home.required' => "El campo Dirección es requerido",
      'phone.digits' => 'Tu Numero Telefonico debe tener El siguiente Formato 04160000000',
      'password.confirmed'=>'Las contraseñas no coindicen',
      'name_shop_slug.unique'=>'La dirección web de la tienda ya ha sido tomado por favor ingrese otro '
      ];
// dd($data);
        if ($data['typeUser'] == '0') {
           return Validator::make($data, [
            // 'typeUser '=>'numeric|required',
            'typeClient'=>'required',
            'document'=>'string|required|min:6|unique:users',
            'address_home' => 'max:255|required|string',
            'phone'=>'digits:11|required|numeric|unique:users',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            ],$messages); 
        }
        elseif ($data['typeUser'] == '1') {
        old('slugify', $data['slugify']);
        return Validator::make($data, [
        //  'code','fiscal',
        // 'document','reference','adress_home','adress_work',
        // 'state','country','city','phone','cellphone','fax',
            // 'typeUser '=>'required',
            'typeClient'=>'required',
            'document'=>'string|required|min:6|unique:users',
            'name_shop'=>'max:255|string|required',
            'address_home' => 'max:255|required|string',
            'phone'=>'digits:11|required|numeric|unique:users',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'name_shop_slug' => 'max:255|required|unique:users',
            'password' => 'required|min:6|confirmed',
        ],$messages);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {  if ($data['typeUser'] == "0") {
      return User::create([
            'typeUser' => $data['typeUser'],
            'typeClient' => $data['typeClient'],
            'document' => $data['document'],
            'name' => $data['name'],
            'phone'=>$data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']), ]);
    } 
    elseif($data['typeUser'] == "1") {
        return User::create([
            'typeUser' => $data['typeUser'],
            'typeClient' => $data['typeClient'],
            'document' => $data['document'],
            'name' => $data['name'],
            'phone'=>$data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),

            
            'name_shop' => $data['name_shop'],
            'name_shop_slug' => str_slug($data['slugify'], '_'),
             'is_buy'=> isset($data['is_buy']) ? true : false,
            'is_shop'=> isset($data['is_shop']) ? true : false,
            'is_ticket'=> isset($data['is_ticket']) ? true : false,
            'is_sell'=> isset($data['is_sell']) ? true : false,
            'address_home'=>$data['address_home'],
        ]);


    }

    }
    public function showRegistrationType() { return view('auth.registerType');}
    public function showRegistrationForm($type="0") {
      return view('auth.registert1',compact('type'));   
    }
}
