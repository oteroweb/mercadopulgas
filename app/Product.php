<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model

{

	protected $fillable = [
							'code',
							'category_id',
							'promotional_title',
							'slug',
							'promotional_subtitle',
							'disponibility',
							'name',
							'price_sell',
							'description',
							'is_new',
							'publishmp',
							'publish',
							'picture1',
							'user_id',
							'published_shop',
							'owsoftware'
							];
           
    // public function seller() { // return $this->hasOne('App\User'); 
    //     return $this->belongsTo('App\User'); }
    public function user()
    {
        // return $this->hasOne('App\User');
        return $this->belongsTo('App\User');
    }

        public function category()
    {
        return $this->belongsTo('App\Category');
    }


        public function questions()
    {
        return $this->hasMany('App\Question');
    }
    

}
