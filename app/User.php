<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use App\Notifications\MyOwnResetPassword as ResetPasswordNotification;
use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable implements  JWTSubject
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'typeUser', 
        'typeClient', 
        'name', 
        'email', 
        'password',
        'is_buy',
        'is_shop',
        'is_sell',
        'is_ticket',
        'code',
        'fiscal',
        'name_shop',
        'document',
        'reference',
        'address_home',
        'address_work',
        'template',
        'state',
        'country',
        'city',
        'phone',
        'cellphone',
        'fax',
        'name_shop_slug',
        'options',
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    // protected $attributes = array(
    //         'options' => '{}'
    //     );
    public function products()
    {
        return $this->hasMany('App\Product');
    }
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
	{
    $this->notify(new ResetPasswordNotification($token));
	}

        public function getJWTIdentifier()
    {
        return $this->id;
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
