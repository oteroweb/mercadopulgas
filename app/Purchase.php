<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
   protected $fillable = [  'quantity',
							'product_id',
							'seller_id',
							'seller_comment',
							'seller_rate',
							'buyer_id',
							'buyer_rate',
							'buyer_comment',
							];
// public function seller() { return $this->hasOne('App\Product', 'seller_id','id'); }
    public function seller() { return $this->hasOne('App\Product', 'seller_id','id'); }
    public function buyer() { return $this->hasOne('App\Product', 'buyer_id','id'); }
    public function product() { return $this->hasOne('App\Product', 'product_id','id'); }	
}
