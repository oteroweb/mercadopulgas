<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Session\TokenMismatchException;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // resolucion fallo de tamaño de archivo
        if ($exception instanceof PostTooLargeException) {
            Session::flash('msg', "Special message goes here");
                return redirect()->back()->with(['error', 'Archivo demasiado grande!']);
        }
        if ($exception instanceof TokenMismatchException) {
                // return redirect()->back()->with(['error', 'Archivo demasiado grande!']);
            // return \Response::make("peticion invalida, vuelta al inicio");
            return \Response::make(view('errors.404'), 404);
        }
       if ($exception instanceof MethodNotAllowedHttpException) {
                // return redirect()->back()->with(['error', 'Archivo demasiado grande!']);
            // return \Response::make("peticion invalida, vuelta al inicio");
            return \Response::make(view('errors.404'), 404);
        }

        if ( !config('app.debug') && !$this->isHttpException($exception)) {
            // return \Response::make(view('errors.404'), 404);
            return \Response::make(view('errors.500'), 404);
            // echo "hola";
        }
            // return \Response::make(view('errors.404'), 404);
// return var_dump($exception);   
        return parent::render($request, $exception);
        // #pendiente para produccion
        //  return view('errors.500');
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        // return redirect()->guest(route('login'));
        return redirect(route('login'))->with('error', 'Por favor iniciar sesion');

    }
}
