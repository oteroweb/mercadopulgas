<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	protected $fillable = ['question','answer','user_id','product_id'];
       public function Product()
    {
        return $this->hasOne('App\Product');
    }
}
