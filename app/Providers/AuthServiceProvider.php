<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-product', function ($user, $product) {
            return $user->id == $product->user_id;
        });
        // Gate::define('update-post', 'PostPolicy@update');

        // if (Gate::allows('update-product', $product)) {
        //  // The current user can update the product...
        // }

        // if (Gate::denies('update-product', $product)) {
        //  // The current user can't update the post...
        // }

    }
}
