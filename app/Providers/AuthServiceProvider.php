<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Gate::define('update-product', function ($user, $product) {
        //     return $user->id == $product->user_id;
        // });
        // Gate::define('view.dashboard', function ($user) {
        //   if ($user->template == null) { return false;}
        //   return true;
        // });
          // if(Gate::denies('view-dashboard')){
          //   redirect('/tablero/plantilla');
          //   // redirect()->route('generate.shop');
          //   return true;
          // }
        //    if(!Auth::user('view-dashboard')) {
      //     dd("hola");
      //       // session(['msg' => 'Para acceder a su tablero debe Primero seleccionar un Tema']);
      //        return redirect()->route('generate.shop');
      //    }
      //     dd("chao");
      // 

    }

    // public function registerPostPolicies()
    // {
    //     Gate::define('create-post', function ($user) {
    //         return $user->hasAccess(['create-post']);
    //     });
    //     Gate::define('update-post', function ($user, Post $post) {
    //         return $user->hasAccess(['update-post']) or $user->id == $post->user_id;
    //     });
    //     Gate::define('publish-post', function ($user) {
    //         return $user->hasAccess(['publish-post']);
    //     });
    //     Gate::define('see-all-drafts', function ($user) {
    //         return $user->inRole('editor');
    //     });
    // }

}
