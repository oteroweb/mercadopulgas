-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-06-2017 a las 20:50:58
-- Versión del servidor: 5.6.35-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `mp_2016`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE IF NOT EXISTS `articulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `articulo` varchar(255) NOT NULL,
  `subtitulo` text,
  `foto1` varchar(255) DEFAULT NULL,
  `foto2` varchar(255) DEFAULT NULL,
  `estado` varchar(255) NOT NULL,
  `ventas` int(5) NOT NULL,
  `stock` int(3) NOT NULL,
  `precio` float NOT NULL,
  `tipopago` varchar(255) NOT NULL,
  `id_usu` int(11) NOT NULL,
  `descripcion` text,
  `garantia` varchar(30) NOT NULL,
  `tipopublicacion` varchar(255) NOT NULL,
  `fechaexp` date NOT NULL,
  `fechainc` date NOT NULL,
  `id_cat` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cat` (`id_cat`),
  KEY `id_usu` (`id_usu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `articulo`, `subtitulo`, `foto1`, `foto2`, `estado`, `ventas`, `stock`, `precio`, `tipopago`, `id_usu`, `descripcion`, `garantia`, `tipopublicacion`, `fechaexp`, `fechainc`, `id_cat`) VALUES
(1, 'Vendo HP 3050 CON Sistema de tinta SIN cartuchos, lista para usar!', NULL, '63851231.jpg', NULL, '1231', 0, 1, 32000, '', 3, '<p>A la venta Impresora HP 3050 100% OPERATIVA VENDO POR NO USAR</p>\r\n\r\n<p>Impresora de gran calidad, tiene conectividad WIFI</p>\r\n\r\n<p>La vendo con los tanques del sistema de tinta LLENO valorado en 10.000bs&nbsp;</p>\r\n\r\n<p>Solamente necesita comprarle cartuchos para adaptarselo en el sistema que ya esta instalado</p>\r\n\r\n<p>en 32000 bs NEGOCIABLE</p>\r\n', '0', '', '0000-00-00', '0000-00-00', 8),
(2, 'Cargador múltiple', NULL, '9961cargador 3.jpg', NULL, '123', 0, 1, 25000, '', 4, '<p><span style="color:rgb(0, 0, 0)">Pr&aacute;ctico cargador m&uacute;ltiple que funciona como central el&eacute;ctrica de pared incluye RND 3 enchufes de corriente alterna y 2 puertos USB con protecci&oacute;n contra sobretensiones. Tambi&eacute;n incluye 2 soportes deslizables a los lados para su smartphone.</span></p>\r\n', 'no', '', '0000-00-00', '0000-00-00', 14),
(3, 'Base monitor', NULL, '150media-20150831 (1).jpg', NULL, '123', 0, 2, 3500, '', 1, '<p>base para monitores 3000 bolivares</p>\r\n', 'Buenas condiciones ', '', '0000-00-00', '0000-00-00', 14),
(4, 'chevrolet oldsmobile del 69', NULL, '532113316811_10154341608774756_6880771098241270562_o.jpg', NULL, '1231', 0, 1, 2400, '', 5, '<p>04145366573</p>\r\n\r\n<p>Barquisimeto</p>\r\n', 'no', '', '0000-00-00', '0000-00-00', 20),
(5, 'Cable Coaxial Rg6 para Directv - 25 Metros', NULL, '578014289958_10209903878613509_2873795132877284474_o.jpg', NULL, '123', 0, 1, 12000, '', 10, '<p>Se Venden 25 Metros de Cable Coaxial RG-6 para Directv&nbsp;</p>\r\n\r\n<p>Especificaciones que trae el cable:</p>\r\n\r\n<p>RG-6 75 OHM High Performance 3.0 GHZ Digital Cable</p>\r\n\r\n<p>18 AWG 75&ordm;C CATV/CL2 E232510 (UL) CM&nbsp;</p>\r\n\r\n<p>Perfect Vision 80 SC</p>\r\n\r\n<p>Precio 12mil los 25 Metros</p>\r\n', '1', '', '0000-00-00', '0000-00-00', 14),
(6, 'LNB Circular Dual Para Directv', NULL, '129214291887_10209868006196721_9185960098532917346_n.jpg', NULL, '123', 0, 1, 4000, '', 10, '<p>Se Vende LNB (Low Noise Block o Bloque de Bajo Ruido) Dual para antena de Directv Dual porque sirve para conectar dos decodificadores nuevo,porque se compro un deco y venia con la antena la cual no se uso por ya tener otra puesta<br />\r\nPrecio 4 mil bs</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1', '', '0000-00-00', '0000-00-00', 14),
(7, 'EN VENTA CAMISAS EPK USADAS UNA VEZ EN OFERTA LAS 2 X 7MILBF ', NULL, '4173IMG_20160922_140922.jpg', NULL, '1231', 0, 2, 7000, '', 12, '<p><span style="color:rgb(29, 33, 41)">EN VENTA CAMISAS EPK USADAS UNA VEZ EN OFERTA LAS 2 X 7MILBF LA ROJA TIENE SOLO UN DETALLITO SALE EN LA FOTO ESTAN NUEVESITAS INFO AL 04161250078 SOLO WHATSSAP TALLAS&nbsp;</span>3 y 5</p>\r\n', '15 DIAS', '', '0000-00-00', '0000-00-00', 18),
(8, 'EN VENTA CAMISAS EPK USADAS UNA VEZ EN OFERTA LAS 2 X 7MILBF ', NULL, '8496IMG_20160922_140846.jpg', NULL, '1231', 0, 1, 7000, '', 12, '<p><span style="color:rgb(29, 33, 41)">EN VENTA CAMISAS EPK USADAS UNA VEZ EN OFERTA LAS 2 X 7MILBF LA ROJA TIENE SOLO UN DETALLITO SALE EN LA FOTO ESTAN NUEVESITAS INFO AL 04161250078 SOLO WHATSSAP </span></p>\r\n', '15 DIAS', '', '0000-00-00', '0000-00-00', 18),
(9, 'Torero Tejido', NULL, '1545IMG_20150804_152405.jpg', NULL, '123', 0, 1, 7500, '', 6, '<p><span style="color:#FF0000"><strong>Torero Tejido a Mano rosado 100% hilos de la mejor calidad no desti&ntilde;e ni pierde su forma al lavarlo</strong></span></p>\r\n', 'original y nuevo', '', '0000-00-00', '0000-00-00', 18),
(10, 'Zapaticos para bebe', NULL, '1372IMG_20150804_115349.jpg', NULL, '123', 0, 1, 1800, '', 6, '<p><span style="color:#EE82EE font-weight:20px">Zapatillas para beb&eacute; talla M</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', 'originales nuevos', '', '0000-00-00', '0000-00-00', 2),
(11, 'Zapaticos para Bebé tejidos a mano', NULL, '5968Img29_06-08-2015.JPG', NULL, '123', 0, 1, 2200, '', 6, '<p>Zapaticos tejidos para beb&eacute; reci&eacute;n nacido en color crema</p>\r\n', 'originales nuevos', '', '0000-00-00', '0000-00-00', 2),
(12, 'Collar artesanal de madera', NULL, '3508IMG_20150804_105824.jpg', NULL, '123', 0, 1, 4500, '', 6, '<p>Collar Artesanal Realizado en Madera real colores vibrantes y a la moda</p>\r\n', 'original', '', '0000-00-00', '0000-00-00', 17),
(13, 'Combos Escolares ', NULL, '5784IMG_20160830_124805.jpg', NULL, '123', 0, 2, 2200, '', 6, '<p><strong>Combos escolares consta de:</strong></p>\r\n\r\n<ul>\r\n	<li>5 lapices mongol</li>\r\n	<li>1 liquid paper o corrector</li>\r\n	<li>1 caja de clips</li>\r\n	<li>1 libreta</li>\r\n	<li>1 borra</li>\r\n</ul>\r\n', 'nuevos', '', '0000-00-00', '0000-00-00', 16),
(14, 'Carteritas de Cuero Colombiano', NULL, '4896Img06_06-08-2015.JPG', NULL, '123', 0, 3, 4900, '', 6, '<p>Set de Carteritas de cuero colombiano se venden juntas o por separado juntas al precio publicado por separado en 1900 c/u</p>\r\n', 'nuevas', '', '0000-00-00', '0000-00-00', 18),
(15, 'Collar brazalete y anillo tejidos', NULL, '3233collares.jpg', NULL, '123', 0, 3, 9800, '', 6, '<p>Collar brazalete y anillo tejidos a mano bellos y a la moda en amarillo fluorscente todos al precio publicado tambien est&aacute;n a la venta individual el set completo est&aacute; a muy buen precio y lo mejor de todo es que son piezas &uacute;nicas exclusivas de la marca</p>\r\n', 'originales', '', '0000-00-00', '0000-00-00', 17),
(16, 'Camisa para dama transparente ', NULL, '692.jpg', NULL, '123', 0, 1, 4500, '', 6, '<p>Talla M &uacute;nica pieza en existencia aprovecha nuestros precios de descuentos y entra ya en nuestra tienda online para ver m&aacute;s productos <a href="https://www.facebook.com/commerce/products/1226311864094113/" onclick="window.open(this.href, '''', ''resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no''); return false;">TIENDA ONLINE</a></p>\r\n', 'NUEVA', '', '0000-00-00', '0000-00-00', 18),
(17, 'Porta jarras antiresbalante tejido', NULL, '165115.jpg', NULL, '123', 0, 2, 1900, '', 6, '<p>Porta jarras antiresbalante tejido en dos presentaciones o colores perfectos para obsequiar es un producto original y novedoso</p>\r\n', 'nuevos originales', '', '0000-00-00', '0000-00-00', 4),
(18, 'Blu R1 HD, 16gb de mem. interna, 2gb RAM, SO Android 6.0 QuadCore 1.3ghz Dual Sim', NULL, '209314604855_1165878250148556_9146037887017068497_n.jpg', NULL, '123', 0, 1, 125000, '', 3, '<p style="text-align:center"><u><strong>Caracteristicas del Equipo:</strong></u></p>\r\n\r\n<p style="text-align:center">-Doble Sim<br />\r\n-Procesador QuadCore a 1.3Ghz<br />\r\n-2GB de Memoria RAM<br />\r\n-16GB de Memoria Interna&nbsp;<br />\r\n-Camara trasera de 8MP+Flash&nbsp;<br />\r\n-Camara delantera 5MP+Flash&nbsp;<br />\r\n-Pantalla 5&quot; Full HD&nbsp;<br />\r\n-Memoria Expandible hasta 64GB</p>\r\n', '0', '', '0000-00-00', '0000-00-00', 8),
(19, 'Blu R1 HD, 8gb de mem. interna, 1gb RAM, SO Android 6', NULL, '769214604855_1165878250148556_9146037887017068497_n.jpg', NULL, '123', 0, 1, 135000, '', 3, '<p style="text-align: center;"><u><strong>Caracteristicas Principales:</strong></u></p>\r\n\r\n<p style="text-align: center;">-Doble Sim<br />\r\n-Procesador QuadCore a 1.3Ghz<br />\r\n-1GB de Memoria RAM<br />\r\n-8GB de Memoria Interna&nbsp;<br />\r\n-C&aacute;mara trasera de 8MP+Flash&nbsp;<br />\r\n-C&aacute;mara delantera 5MP+Flash&nbsp;<br />\r\n-Pantalla 5&quot; Full HD&nbsp;<br />\r\n-Memoria Expandible hasta 64GB</p>\r\n', '0', '', '0000-00-00', '0000-00-00', 7),
(20, 'jornada especial ginecologica ', NULL, '', NULL, '123', 0, 15, 6000, '', 15, 'Eco vaginal eco pelvico eco mamario toma de la citologia colposcopia consulta ', 'si', '', '0000-00-00', '0000-00-00', 19),
(21, 'Diseño Gráfico De Logos Para Tu Empresa', NULL, '8083logoemail.jpg', NULL, '123', 0, 10, 100, '', 6, '<p style="text-align: center;"><img alt="" src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/fef0a643684215.57f85f35aaf9d.jpg" style="float:right; height:3333px; width:700px" /></p>\r\n', 'Orginales', '', '0000-00-00', '0000-00-00', 20),
(24, 'Botas de cuero', NULL, '', NULL, '1231', 0, 1, 15000, '', 18, '', 'una sola postura', '', '0000-00-00', '0000-00-00', 18),
(25, 'silla de carro para bebe', NULL, '42IMG-20160926-WA0000.jpg', NULL, '1231', 0, 1, 20, '', 28, 'Usada en buen estado', 'ninguna', '', '0000-00-00', '0000-00-00', 2),
(26, 'lapto', NULL, '136scholarship.png', NULL, '1231', 0, 1, 2000, '', 31, '<p>HOLA&nbsp;</p>\r\n', '2 meses', '', '0000-00-00', '0000-00-00', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(255) DEFAULT NULL,
  `padre` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `padre`, `status`) VALUES
(1, 'Accesorios para Vehículos', 0, '1'),
(2, 'Bebés', 0, '1'),
(3, 'Animales y Mascotas', 0, '1'),
(4, 'Hogar y Muebles', 0, '1'),
(5, 'Industrias', 0, '1'),
(6, 'Instrumentos Musicales', 0, '1'),
(7, 'Cámaras y Accesorios', 0, '1'),
(8, 'Celulares y Teléfonos', 0, '1'),
(9, 'Coleccionables y Hobbies', 0, '1'),
(10, 'Computación', 0, '1'),
(11, 'Consolas y Videojuegos', 0, '1'),
(12, 'Deportes y Fitness', 0, '1'),
(13, 'Electrodomésticos', 0, '1'),
(14, 'Electrónica, Audio y Video', 0, '1'),
(15, 'Juegos y Juguetes', 0, '1'),
(16, 'Libros, Música y Películas', 0, '1'),
(17, 'Relojes, Joyas y Bisutería', 0, '1'),
(18, 'Ropa, Zapatos y Accesorios', 0, '1'),
(19, 'Salud y Belleza', 0, '1'),
(20, 'Otras Categorías', 0, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `slug`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Accesorios para Vehículos', 0, 'vehiculos-accesorios', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(2, 'Alimentos y Bebidas', 0, 'alimentos-bebidas', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(3, 'Animales y Mascotas', 0, 'animales-mascotas', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(4, 'Antigüedades', 0, 'antiguedades', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(6, 'Arte y Artesanías', 0, 'arte-artesanias', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(7, 'Autos, Motos y Otros', 0, 'vehiculos', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(8, 'Bebés', 0, 'bebes', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(9, 'Cámaras y Accesorios', 0, 'camaras', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(10, 'Celulares y Teléfonos', 0, 'celulares-telefonos', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(11, 'Coleccionables y Hobbies', 0, 'coleccionables-hobbies', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(12, 'Computación', 0, 'computacion', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(13, 'Consolas y Videojuegos', 0, 'consolas-videojuegos', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(14, 'Deportes y Fitness', 0, 'deportes-fitness', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(15, 'Electrodomésticos y Aires Ac.', 0, 'electrodomesticos', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(16, 'Electrónica, Audio y Video', 0, 'electronica', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(17, 'Entradas para Eventos', 0, 'eventos', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(18, 'Herramientas y Construcción', 0, 'herramientas-contruccion', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(19, 'Hogar, Muebles y Jardín', 0, 'hogar', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(20, 'Industrias y Oficinas', 0, 'industrias-oficinas', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(21, 'Inmuebles', 0, 'inmuebles', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(22, 'Instrumentos Musicales', 0, 'instrumentos-musicales', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(23, 'Joyas y Relojes', 0, 'joyas-relojes', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(24, 'Juegos y Juguetes', 0, 'juegos-juguetes', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(25, 'Libros, Revistas y Comics', 0, 'libros-revistas-comics', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(26, 'Música, Películas y Series', 0, 'musica-pelicula-series', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(27, 'Ropa y Accesorios', 0, 'ropa-accesorios', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(28, 'Salud y Belleza', 0, 'salud-belleza', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(29, 'Servicios', 0, 'servicios', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL),
(30, 'Otras categorías', 0, 'otros', 1, '2017-05-13 23:46:55', '2017-05-13 23:46:55', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1472966616),
('m140209_132017_init', 1472966618),
('m140403_174025_create_account_table', 1472966618),
('m140504_113157_update_tables', 1472966618),
('m140504_130429_create_token_table', 1472966618),
('m140830_171933_fix_ip_field', 1472966618),
('m140830_172703_change_account_table_name', 1472966618),
('m141222_110026_update_ip_field', 1472966618),
('m141222_135246_alter_username_length', 1472966618),
('m150614_103145_update_social_account_table', 1472966618),
('m150623_212711_fix_username_notnull', 1472966618),
('m151218_234654_add_timezone_to_profile', 1472966619),
('m130524_201442_init', 1472966632),
('m151106_040101_articulosv1', 1472966632),
('m151124_185727_registro', 1472966632),
('m151207_040343_preguntas', 1472966632);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2017_04_10_033538_laratrust_setup_tables', 1),
(14, '2017_04_10_040247_create_categories_table', 1),
(15, '2017_04_10_080233_create_products_table', 1),
(16, '2017_04_27_025503_create_questions_table', 1),
(17, '2017_04_28_052528_add_publish_to_users_table', 1),
(21, '2017_04_28_052538_add_publishmp_to_users_table', 2),
(22, '2017_04_29_085824_create_purchases_table', 2),
(23, '2017_04_29_085838_create_reputations_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('oterolopez1990@gmail.com', '$2y$10$2hcIdIEX6qMALP92xMQS2.nxw54JNjw6nBCsMjdJOw3hhP8kAIOA.', '2017-05-14 13:19:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_user`
--

CREATE TABLE IF NOT EXISTS `permission_user` (
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`user_id`,`user_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE IF NOT EXISTS `preguntas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pregunta` varchar(255) NOT NULL,
  `respuesta` varchar(255) DEFAULT NULL,
  `id_pre` int(11) NOT NULL,
  `id_rep` int(11) DEFAULT NULL,
  `id_art` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pre` (`id_pre`),
  KEY `id_rep` (`id_rep`),
  KEY `id_art` (`id_art`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id`, `pregunta`, `respuesta`, `id_pre`, `id_rep`, `id_art`) VALUES
(1, 'hola buenas no vdndes los decodificadores', NULL, 6, NULL, 6),
(2, 'entrega factura?', NULL, 6, NULL, 1),
(3, 'hola puede colocar mas foto del articulo', NULL, 6, NULL, 3),
(4, 'hola no sale la otra foto', NULL, 6, NULL, 7),
(5, 'en cuanto?', NULL, 31, NULL, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nombre',
  `name_short` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nombre corto',
  `mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'sin marca',
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'sin modelo',
  `deparment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'departamento',
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'sin referencia',
  `days_guarante` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'dias garantia',
  `promotional_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Promocion por defecto',
  `is_new` tinyint(1) NOT NULL DEFAULT '1',
  `sales` int(11) NOT NULL DEFAULT '0',
  `unit` int(11) NOT NULL DEFAULT '0',
  `fee_buy` double(15,8) NOT NULL DEFAULT '0.00000000',
  `fee_sell` double(15,8) NOT NULL DEFAULT '0.00000000',
  `price_sell` double(15,8) NOT NULL DEFAULT '0.00000000',
  `decimal_expresed` int(11) NOT NULL DEFAULT '2',
  `cost_method` double(15,8) NOT NULL DEFAULT '0.00000000',
  `cost_provider` double(15,8) NOT NULL DEFAULT '0.00000000',
  `cost_calculate` double(15,8) NOT NULL DEFAULT '0.00000000',
  `cost_last` double(15,8) NOT NULL DEFAULT '0.00000000',
  `cost_average` double(15,8) NOT NULL DEFAULT '0.00000000',
  `price_offer` double(15,8) NOT NULL DEFAULT '0.00000000',
  `price_max` double(15,8) NOT NULL DEFAULT '0.00000000',
  `price_min` double(15,8) NOT NULL DEFAULT '0.00000000',
  `price_mayor` double(15,8) NOT NULL DEFAULT '0.00000000',
  `promotional_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Titulo Articulo',
  `promotional_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Subtitulo Articulo',
  `picture1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disponibility` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guarantee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `other_cost` double(15,8) DEFAULT NULL,
  `cost_ship` double(15,8) DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publishmp` tinyint(1) NOT NULL DEFAULT '0',
  `published_shop` tinyint(1) NOT NULL DEFAULT '0',
  `owsoftware` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`),
  KEY `products_user_id_foreign` (`user_id`),
  KEY `products_category_id_foreign` (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `name_short`, `mark`, `model`, `deparment`, `reference`, `days_guarante`, `promotional_type`, `is_new`, `sales`, `unit`, `fee_buy`, `fee_sell`, `price_sell`, `decimal_expresed`, `cost_method`, `cost_provider`, `cost_calculate`, `cost_last`, `cost_average`, `price_offer`, `price_max`, `price_min`, `price_mayor`, `promotional_title`, `promotional_subtitle`, `picture1`, `category_id`, `user_id`, `slug`, `picture2`, `disponibility`, `description`, `guarantee`, `created_at`, `updated_at`, `deleted_at`, `other_cost`, `cost_ship`, `publish`, `publishmp`, `published_shop`, `owsoftware`) VALUES
(1, 'Jettie Murray', 'nombre', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 1, 0, 0, 0.00000000, 0.00000000, 9.99000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'promotional_title Eda Torphy', 'promotional_subtitle Eda Torphy', NULL, 20, 3, 'eda_torphy', 'http://lorempixel.com/640/480/?24098):', 4, 'description Eda Torphy', NULL, '2017-05-13 23:46:59', '2017-05-13 23:46:59', NULL, NULL, NULL, 1, 0, 0, 0),
(2, '128', 'Eduplay33YTaa', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 0, 0, 0, 0.00000000, 0.00000000, 222.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'Nuevamente Stock', 'subtitulo con stockactualiado', 'eYtYHlWkadMAtoskwa7kCIj5IB2zqr6E42rglGBc.jpeg', 3, 1, '857_titulo_promocional_con_stock', NULL, 32, 'sa1ratoga', NULL, '2017-05-14 13:34:56', '2017-05-16 18:04:48', NULL, NULL, NULL, 0, 0, 0, 0),
(3, '643', 'nombre solo MP', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 0, 0, 0, 0.00000000, 0.00000000, 14.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'Titulo promocional con stock solo mp', 'subtitulo con stock', NULL, 1, 1, '165_titulo_promocional_con_stock_solo_mp', NULL, 3, 'solo mp', NULL, '2017-05-14 14:28:42', '2017-05-14 14:28:42', NULL, NULL, NULL, 0, 1, 0, 0),
(4, '204', 'solo tienda', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 0, 0, 0, 0.00000000, 0.00000000, 32.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'solo tienda', 'solo tienda', NULL, 1, 1, '964_solo_tienda', NULL, 2, 'solo tienda', NULL, '2017-05-14 14:29:14', '2017-05-14 14:29:14', NULL, NULL, NULL, 1, 0, 0, 0),
(5, '693', 'en todos', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 0, 0, 0, 0.00000000, 0.00000000, 2.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'en todos', 'en todos', NULL, 1, 1, '361_en_todos', NULL, 23, 'en todos', NULL, '2017-05-14 14:29:59', '2017-05-14 14:29:59', NULL, NULL, NULL, 1, 1, 0, 0),
(6, '315', 'solo en mp', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 0, 0, 0, 0.00000000, 0.00000000, 2.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'solo en mp', 'solo en mp', NULL, 1, 1, '944_solo_en_mp', NULL, 2, 'solo en mp', NULL, '2017-05-14 14:30:28', '2017-05-14 14:30:28', NULL, NULL, NULL, 0, 1, 0, 0),
(7, '746', 'todos real', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 1, 0, 0, 0.00000000, 0.00000000, 3.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'todos real', 'todos real', NULL, 1, 1, '581_todos_real', NULL, 2, 'todos real', NULL, '2017-05-14 14:53:18', '2017-05-14 14:53:18', NULL, NULL, NULL, 0, 1, 0, 0),
(8, '721', 'todos real 3', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 0, 0, 0, 0.00000000, 0.00000000, 0.17000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'todos real 3', 'todos real 3', NULL, 1, 1, '543_todos_real_3', NULL, 3, 'todos real', NULL, '2017-05-14 14:56:26', '2017-05-18 13:03:46', NULL, NULL, NULL, 0, 0, 1, 0),
(9, '173', 'contodo', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 1, 0, 0, 0.00000000, 0.00000000, 10.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'hogar titulo', 'hogar subtitulo', NULL, 19, 1, '676_hogar_titulo', NULL, 5, 'descripcion bien2', NULL, '2017-05-16 14:08:26', '2017-05-16 15:20:08', NULL, NULL, NULL, 0, 1, 1, 1),
(10, '728', 'contodo', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 1, 0, 0, 0.00000000, 0.00000000, 10.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'hogar titulo', 'hogar subtitulo', NULL, 19, 1, '48_hogar_titulo', NULL, 5, 'descripcion bien2', NULL, '2017-05-16 15:10:11', '2017-05-16 15:10:11', NULL, NULL, NULL, 0, 1, 1, 1),
(11, '52', 'probando', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 1, 0, 0, 0.00000000, 0.00000000, 21.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'nuevo articulo repo', 'vender', 'P3nowgzXKVRLc6R3NGsRHsG3EPc7wMNBWPg8tBZX.jpeg', 1, 1, '213_nuevo_articulo_repo', NULL, 22, 'nuevo articulo', NULL, '2017-05-16 16:25:11', '2017-05-16 16:25:11', NULL, NULL, NULL, 0, 0, 0, 0),
(12, '88', 'VIVIENDA UNIFAMILIAR', 'nombre corto', 'sin marca', 'sin modelo', 'departamento', 'sin referencia', 'dias garantia', 'Promocion por defecto', 0, 0, 0, 0.00000000, 0.00000000, 10000.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 'HERMOSA CASA', 'LINDA CASA LISTA PARA HABITAR EN URBANIZACIÓN CERRADA. CABUDARE', 'yPiQijiFyJeoFFSicylUgjbWdXxDXXrOd40JWcYq.png', 1, 5, '290_hermosa_casa', NULL, 1, 'LINDA CASA CON TRES HABITACIONES COCINA SALA COMEDOR PATIO Y PARRILLERA', NULL, '2017-05-21 00:56:52', '2017-05-21 00:58:15', NULL, NULL, NULL, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`) VALUES
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'suzanne', 'svaldive@gmail.com', '', 'd41d8cd98f00b204e9800998ecf8427e', 'Barquisimeto Venezuela', 'http://suzannediseno.com.ve', 'Suzanne Diseño & Marketing es la Agencia de Diseño Gráfico, Mercadeo y Publicidad en Barquisimeto, Venezuela; contamos con más de 10 años de experiencia en el ramo del mercadeo y la publicidad digital, diseño gráfico y diseño de herramientas on line como páginas web, tiendas virtuales, campañas de e-mail y redes sociales. Estamos ubicados en Barquisimeto pero también brindamos todos nuestros servicios de diseño gráfico en el resto de Venezuela y el Mundo.\r\n\r\nEntre nuestros servicios podemos mencionar: Asesoría en Estrategias de Mercadeo, Diseño de Imagen Corporativa, Diseño Gráfico, Diseño de Logotipos, Diseño de Páginas Web, Diseño de Afiches, Diseño de Banners, Diseño de perfiles para Redes Sociales, Diseño de Empaques de Productos, Diseño de Tarjetas de Negocios, Diseño de Señalética y Producción de Videos Corporativos Publicitarios y personales.\r\n\r\nOfrecemos todo tipo de asesorías a pequeñas y medianas empresas, donde nos enfocamos en ofrecer lo mejor, lo actual y lo efectivo en cuanto a captación de clientela se refiere, asesorías para decoración de espacios con bajo presupuesto una alternativa más que impulsamos desde el conocimiento del reciclaje y el que todo no pierde vigencia sino que se puede usar de una forma creativa.\r\n\r\nLe invitamos a visitar nuestra pagina web encontrará un mundo de posibilidades que seguro se adaptarán a algún requerimiento de su interés.', 'America/La_Paz'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `purchases`
--

CREATE TABLE IF NOT EXISTS `purchases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `product_id` int(10) unsigned NOT NULL,
  `seller_id` int(10) unsigned NOT NULL,
  `seller_comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_rate` double DEFAULT '-1',
  `buyer_id` int(10) unsigned NOT NULL,
  `buyer_rate` double DEFAULT '-1',
  `buyer_comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchases_product_id_foreign` (`product_id`),
  KEY `purchases_seller_id_foreign` (`seller_id`),
  KEY `purchases_buyer_id_foreign` (`buyer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `purchases`
--

INSERT INTO `purchases` (`id`, `quantity`, `product_id`, `seller_id`, `seller_comment`, `seller_rate`, `buyer_id`, `buyer_rate`, `buyer_comment`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 12, 4, 1, NULL, -1, 5, -1, NULL, '2017-05-21 01:10:48', '2017-05-21 01:10:48', NULL),
(2, 12, 4, 1, NULL, -1, 5, -1, NULL, '2017-05-21 01:11:11', '2017-05-21 01:11:11', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_product_id_foreign` (`product_id`),
  KEY `questions_user_id_foreign` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer`, `product_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ESTA BUENA ESTA OFERTA?', NULL, 4, 5, '2017-05-21 01:09:49', '2017-05-21 01:09:49', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_account`
--

CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `social_account`
--

INSERT INTO `social_account` (`id`, `user_id`, `provider`, `client_id`, `data`, `code`, `created_at`, `email`, `username`) VALUES
(1, 19, 'facebook', '262621077466873', '{"name":"Enrique Ventura","email":"enriqueventura56@gmail.com","id":"262621077466873"}', NULL, NULL, NULL, NULL),
(2, 23, 'facebook', '10210986781043702', '{"name":"Gleccy Minerva Gaona Rojas","email":"gleccygaona@gmail.com","id":"10210986781043702"}', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(2, 'ZEGoZ7BD31aCgV6jl98zQRtncJIcyF1O', 1472967242, 0),
(3, 'iDHA5QlE-AKsOA8UPZJFWZ6E8uHF3g9r', 1472967991, 0),
(4, 'ZdoKF5oJo8K-9HfBaNB1k7U7jfwQ-HK_', 1473008878, 0),
(5, '5peHfbKXZdBPrFPEJAcTKBuo3OhVg1Hg', 1473517048, 0),
(6, 'C236cOMvys5hST3lPnuIAtuEuCmXhncX', 1473649097, 0),
(7, 'A7TWOxk3V4H5KsCrmF4Zd_9c9s5vDih-', 1473865359, 0),
(8, '2QzZlp5zETNgT9jdHvGD8IggU3Gf4PNd', 1474428124, 0),
(9, 'VlnKNhp1vL0E_ozvjSBuy-MlmAYZZn2a', 1474489823, 0),
(10, 'VI7JZxQEJQiBSc3VBByi0z480yN6MD44', 1474494369, 0),
(11, 'YLosEnrX8eVjQDDtp1mvK_Ts8fDRk6Kv', 1474542474, 0),
(12, '5ytLLYkZN6i_Um2IwmEF09BUSc2I8jAI', 1474588154, 0),
(13, 'uOpsy4tZTADeMtVVj3yx7lun4fLtgjL3', 1475439888, 0),
(14, 'TsSe74giZnSkzTJY2yG2tfWrwFtZKV_H', 1475784054, 0),
(15, 'HfFuzYHoKOv5N4g4wEiaUDqqUctubPzB', 1475956402, 0),
(16, '0RjE0o4ohTMs-l7FqMrSrG1ZgrgT-K2d', 1475957962, 0),
(17, 'xiw6vd36J36voWz0IkpINrpqJn027H3S', 1479527417, 1),
(17, 'YO8cZGJ4faAuwbM7hmN1wemmWlL6WZC8', 1475960484, 0),
(18, 'q4f9JYYL2GkDnWwtU09aMn_DxZEJfghC', 1475969733, 0),
(20, '7-A_v3JGVaQt-hsM6VfpNDNIZZSBhW25', 1476058792, 0),
(21, 'e4vffsrWP6Tb4CjUmGtkZFloXAhcplTI', 1476147731, 0),
(22, 'NA7rQk6pcGpTTyKo008noJzVhByiL8aZ', 1476215458, 0),
(24, 'allrZXPaSxnfX7nCoSseLZaUsWW6FdI2', 1477765689, 0),
(26, 'qI4CQKcAvyNWyXQ3jvcE2xrnE8nZyq3I', 1479423216, 0),
(27, 'dRi1poTeefa0QrCMx8-BuDd3SS74X-Bn', 1480640948, 0),
(28, '53Uj7IqybPouUNUYuL6DGBoUIQ5Pyi_f', 1480884975, 0),
(29, 'Qskm1qU2BNe7Xnc0Yyql8clGpcjBoovb', 1487835528, 0),
(30, '7n8pfGbwpG304d3Rv3vOAUK8Hi_Eb8kc', 1488021096, 0),
(31, 'FMc7Esygd2aBnOKukjRYjeoS3jFXW-hF', 1493041828, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_email` (`email`),
  UNIQUE KEY `user_unique_username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `status`) VALUES
(1, 'jilio1990', 'admin@mercadopulgas.com.ve', '$2y$13$Uk1Pek8P9Hasqd.R.ZJsHOYlnDiHY0TKbJZe8IPt3oU6g73R5Dau2', '5E08CYOZSsESSOjP0mpAcM7KiPgIsRhx', NULL, NULL, NULL, NULL, 1447228597, 1447228597, 0, 10),
(2, 'test1', 'test1@test.com', '$2y$10$5xFYRykGTthUKUDjnB.sFO7VGnYb52twXdxrD8G9wrWWgh9I0AC8W', 'cKsS5AgKt_HPRrtxzcj0DE4-HK_YlUGF', NULL, NULL, NULL, '190.142.102.119', 1472967242, 1472967242, 0, 10),
(3, 'josmanmx', 'encpagjos@gmail.com', '$2y$10$ztytdUPV2/qHVYkNR4X6wOV54huaaTlXwfqEs59vDbYy2PWCibI2S', 'nRvYSrDEKCe78nqZSFxGoGeIBA_Z4DQo', NULL, NULL, NULL, '190.200.130.245', 1472967991, 1472967991, 0, 10),
(4, 'karengarcia', 'lucigarcia1953@gmail.com', '$2y$10$sEfgiermKUAcQYa6LuJDWOuYBJsC62WNbg38GKzHshMqYBUsCGjpy', 'r2PlItp1-45pZ_OHsRod8LYvwb7aR0lY', NULL, NULL, NULL, '186.91.97.7', 1473008878, 1473008878, 0, 10),
(5, 'enriquesangil', 'enriquesapphire2@hotmail.com', '$2y$10$IiGDlcYlKQ5Ehnzn0Q0YZu40VtkJ5YSSMvA6HOZrc.do8uw0txDzi', 'Co6Elpzv-J5I_ameJpdiVQYLxLoJmYSZ', NULL, NULL, NULL, '201.243.22.100', 1473517048, 1473517048, 0, 10),
(6, 'suzanne', 'svaldive@gmail.com', '$2y$10$kSon.umbGfitV7NwCO1MqOSAaUABoAN3BHqjd271Ky.LdOF6OKDbG', 'D6voddG378a7PU7qGeYnM9U0DQPm951e', NULL, NULL, NULL, '201.248.221.72', 1473649097, 1473649097, 0, 10),
(7, 'HelenaPCh', 'olgaperez2828@gmail.com', '$2y$10$mNh5gdAcBrq3dX3W9KGfJ..HS9ZCn2msRVZQERx5KXZWHzYYYVAde', 'NjwEdypZLUSFPiP6sj4cJnRam4112FDV', NULL, NULL, NULL, '201.248.211.248', 1473865359, 1473865359, 0, 10),
(8, 'bricenostattoo', 'bricenostattoo@gmail.com', '$2y$10$ZsxsQI3KJLnF1Yf3qm2W9OBdntP7dkt1EePWrV7i.pfkp4hBie9q6', 'kiOvjbXwcwXA-ecSg_ZfpIuLWKMOv4q6', NULL, NULL, NULL, '186.91.192.28', 1474428124, 1474428124, 0, 10),
(9, 'Cirglis', 'Cj_mogollon@hotmail.com', '$2y$10$7jPVof72OERxzh/H7GlG1ejYiRYd5ecTaj7ivdOAwtC8lQZzQY3Pi', 'WhE7oPRxe5cFoqVD1gwPGp1weFWVRo6k', NULL, NULL, NULL, '201.248.147.49', 1474489823, 1474489823, 0, 10),
(10, 'willsira', 'robb_nen31@hotmail.com', '$2y$10$nrNqVUTtWIXYhO6oOt8oBOU.xSfYsTbCe164x4lKAUR8HJ8BdLmXe', '8buAyjtUoYN5MDeYeW_iCZ8zzEC8vhdi', NULL, NULL, NULL, '201.249.99.175', 1474494369, 1474494369, 0, 10),
(11, 'luis', 'metalero_02@hotmail.com', '$2y$10$ECazdcJMHe1WFe1v/TfvrOJN5qRbUiTmKvS51lYRj.SMzH/u4OvhS', 'xllO-FEGvqkb3RLLzlC3_bSbuVqNouAh', NULL, NULL, NULL, '190.200.181.73', 1474542474, 1474542474, 0, 10),
(12, 'luisaelena', 'heleen_0401@hotmail.com', '$2y$10$IsUR.D39xraIIZ99VIwTbuRPN1c2xMUSR.qou35ZCfuupUSyiBski', 'S-VMLbNfnWkdy5W2irGcB9Ql8FjUqEy4', NULL, NULL, NULL, '190.79.152.76', 1474588154, 1474588154, 0, 10),
(13, 'RamonPycle', 'ramonrurne@mail.ru', '$2y$10$SHC7iwBKdQsuJ2J3oAUWvutcXsjrMOCU5x5rJbtRJ1QkHthZjLI9C', 'RPv4Kd24iISI_lCK2yycIDzn1Pbf0xsy', NULL, NULL, NULL, '37.115.185.215', 1475439888, 1475439888, 0, 10),
(14, 'CharlesNog', 'charleslof@mail.ru', '$2y$10$7/eC0Tp0qVPAlWI3VSb1neW7D2ZVmbVLMONhivUR0FwieYa5A7RpS', 'ZEAZrDAh-GNQd8ZTI-wAGmP20tFq0qJF', NULL, NULL, NULL, '46.119.113.132', 1475784054, 1475784054, 0, 10),
(15, 'Mabel', 'yuvi.t@hotmail.com', '$2y$10$Nu/RRNHIP6K7JmoQjN9vo.B6iCoZEibRIceEuSDptZfxyNB0b/YBm', 'oiR6gyzbSp0qnCmarCLOg00adq2IOk1_', NULL, NULL, NULL, '190.76.250.52', 1475956402, 1475956402, 0, 10),
(16, 'karenjuliet', 'kjmc0683@gmail.com', '$2y$10$aYB7STaoBqbLPjqpvlDhB.J/GjOSvc4QGbn4VG3uviveqM6yISQ6K', 'AjzzHukP47UfZ9C-PcGgNrP4apHlXPbW', NULL, NULL, NULL, '201.243.2.176', 1475957962, 1475957962, 0, 10),
(17, 'elenaelizabeth', 'elionix88@hotmail.com', '$2y$10$75nSLNSkSjQNZoqoJ7ryc.KujqgUulIOmHqVBhwVuPHTEnVN3HGV2', '-hzINVDd0JX-NqFUVvoahqWbX_Cu4Flj', NULL, NULL, NULL, '201.243.21.16', 1475960484, 1475960484, 0, 10),
(18, 'blancamendoza', 'blancamirosfay@hotmail.com', '$2y$10$0a/oMT8tMZDi3W4lgnHw4OvYLo3MO4LpYqeTyzRV7g3QD2qfyFq9m', 'PBWcCC9Y6Hskz65RXNDUQmOYkthSa0Jp', NULL, NULL, NULL, '186.167.244.102', 1475969733, 1475969733, 0, 10),
(19, 'Enrique', 'enriqueventura56@gmail.com', '$2y$10$1E/u/Ym/MD6yyd5OA7vsrua6a61d7PGObYtbYbbQce2Ws9N4sQflK', 'LmHzgjIRzQcZnEZ8Po2mFrqCpeYTvSJ_', 1476037549, NULL, NULL, '190.76.250.57', 1476037549, 1476037549, 0, 10),
(20, 'Vanessagonzalezc@hotmail.com', 'vanessagonzalezc@hotmail.com', '$2y$10$/EPIIiSnDRDsnLPShg9REuljuS7/CHtuggpcZEfXuvcbz.MpTHw8C', 'gYSnwAx7WzUgm3jKsPeLj0tQcivWw11x', NULL, NULL, NULL, '190.142.66.229', 1476058792, 1476058792, 0, 10),
(21, 'Dayana', 'Dayarivero@outlook.com', '$2y$10$.o7AGekPqsAX/NEIPBleXuaeulQ1OLtNsfLwEWbcLHmFcXSXi8dke', 'GNug2_O69_1xs8PramKSqERt141C0Wvk', NULL, NULL, NULL, '190.207.191.237', 1476147731, 1476147731, 0, 10),
(22, 'eparraga', 'edixonparraga@gmail.com', '$2y$10$S1MUSxVT.PYYWIBVqQFC4OPoYX..KKPQ9blQMPEUS3.j26kCVismS', '6EICyYFImAhvbbzQxmzJvyTs07dENOVC', NULL, NULL, NULL, '201.248.73.83', 1476215458, 1476215458, 0, 10),
(23, 'gleccy', 'gleccygaona@gmail.com', '$2y$10$4C3HQTPybMAEuUEd4TgJQ.kxDMA18gkc0t9cb3zHLeuQkWQD5XoBS', 'cXgWqtAseJA7n6SD2y4D6oUuOYyd-aVF', 1477763380, NULL, NULL, '190.76.250.12', 1477763380, 1477763380, 0, 10),
(24, 'Ismar11', 'morenoismar11@hotmail.com', '$2y$10$XOHWUdqhP4oLnhT5fXlkru.M9LqnQf4oTChdW8KdIzQFxE3.mn42a', '6Uz_GANkDC5uR0Av6COEO4qXvI0c83-b', NULL, NULL, NULL, '186.167.251.208', 1477765689, 1477765689, 0, 10),
(26, 'teplitcaua', 'fasadykiev@mail.ru', '$2y$10$CokvISEs0AtLZE8tSKEh8OZwWMOIUu.Ytmz8nmIqJuOGAVyFHH45e', 'mMkmyd_ktkc6dLTEA4mLtEYbs1kJdm9n', NULL, NULL, NULL, '188.163.96.200', 1479423216, 1479423216, 0, 10),
(27, 'Testing', 'test@mailinator.com', '$2y$10$n.J2FbkFESa1gNhR3BR88uJjjW9/wpWV12tkO/bD6gU6ZBYNvZ5He', 'HRdB8P1lq-Q9x8ufip8OBl8vsS5m9oz7', NULL, NULL, NULL, '186.114.122.253', 1480640948, 1480640948, 0, 10),
(28, 'kdiaz29', 'karale290@hotmail.com', '$2y$10$xYsJZ/Zyp4Uf1ytsTlPhbegr71HPsY0No4GXjE22kAVapI6pa3mKq', 'aY5Mrgnazt1eZNGc7xAJgvjt11elu0-k', NULL, NULL, NULL, '190.207.209.197', 1480884975, 1480884975, 0, 10),
(29, 'raskrutkasite', 'kolya.antipod@mail.ru', '$2y$10$avNG8cPjI1YBVzK.N/0yOusqYkZgkBGPONSVa5aXMlp/fnnT5ybke', 'k48i8oQ1Rs4iebtkkdcCdlcCqFoQw1Cq', NULL, NULL, NULL, '188.163.99.172', 1487835528, 1487835528, 0, 10),
(30, 'greluis', 'greluis@cantv.net', '$2y$10$08UhjhQmtfRnjDr5glPdK.t3SayHgA2oQSgbl61APeqhNX7CBMjKu', 'GG5lm6UTto1X157QGj4HHoe9UAyhBkQo', NULL, NULL, NULL, '190.200.137.20', 1488021096, 1488021096, 0, 10),
(31, 'skate182', 'eduardaguilar.dev@gmail.com', '$2y$10$vT0zmAPCFeidALdV4M6xIOhP/LhTyaO2tcSZLosbwrWF4hyQU8uru', 'kiNt5IrLp1fjPNQGCUFIfJAkmlzwefxs', NULL, NULL, NULL, '64.71.171.64', 1493041828, 1493041828, 0, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 's/n',
  `fiscal` enum('contribuyente','no contribuyente') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_buy` tinyint(1) NOT NULL DEFAULT '0',
  `is_sell` tinyint(1) NOT NULL DEFAULT '0',
  `is_shop` tinyint(1) NOT NULL DEFAULT '0',
  `is_ticket` tinyint(1) NOT NULL DEFAULT '0',
  `name_shop` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_shop_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mercadopulgas',
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `address_home` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `address_work` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Caracas',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Distrito Capital',
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'caracas',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '02510000000',
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '04160000000',
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '02510000000',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_name_shop_slug_unique` (`name_shop_slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `code`, `fiscal`, `name`, `is_buy`, `is_sell`, `is_shop`, `is_ticket`, `name_shop`, `name_shop_slug`, `document`, `template`, `reference`, `address_home`, `address_work`, `state`, `country`, `city`, `phone`, `cellphone`, `fax`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 's/n', 'contribuyente', 'oteroweb', 0, 0, 0, 0, 'oteroweb_shop', 'oteroweb_shop', 'V19708951', 'artico', '0', '0', '0', 'Caracas', 'Distrito Capital', 'caracas', '02510000000', '04160000000', '02510000000', 'oterolopez1990@gmail.com', '$2y$10$WiYtdPsxAP5bkY12KOqWaukeeo8f6Nd1Hm4wUQiqoc7WQdJmNekhO', 'Zlecr9sosrjsbL9z2pPvfVgzQeWOZHxmwaqtlvnJz4jDUTSpKIefgfDuXtTB', NULL, NULL, NULL),
(4, 's/n', 'contribuyente', 'MANUEL ANTONIO YANEZ VILLARROEL', 0, 1, 1, 1, '14 1 3', '14_1_3', 'v19708952', 'artico', '0', 'carrera 18 con calle 28, local 236', '0', 'Caracas', 'Distrito Capital', 'caracas', '04167528772', '04160000000', '02510000000', 'info@oteroweb.com', '$2y$10$e2RjVm0QtZ5UtlZPYvhKLOqsT9zSos2CNGSGAUqOwGDCfYO2MFPXK', '6PPfFbPJXM8Bz2rjcmV4XSgxw5NzzyOjnjstNPil7nbSmoRvRpZd4G8cAYUk', '2017-05-18 12:09:37', '2017-05-18 12:09:37', NULL),
(3, 's/n', 'contribuyente', 'Eugenia Kihn', 0, 0, 0, 0, 'Eugenia Kihnshop', 'eugenia_kihn_shop', '0', '0', '0', '787 Harold Drive Suite 292\nSchmidtshire, OH 23255-2025', '0', 'Caracas', 'Distrito Capital', 'caracas', '02510000000', '04160000000', '02510000000', 'altenwerth.rowena@example.net', '$2y$10$XfafuOqHkPYr0.1stmJWy.gidH8NnXIWdaKlsrhUHa18IzuDqsDSS', 'ZcSeHvuKXQ', '2017-05-13 23:46:59', '2017-05-13 23:46:59', NULL),
(5, 's/n', 'contribuyente', 'VLADIMIR ANTONIO COLMENARES CARDENAS', 1, 1, 1, 1, 'HIPEERS', 'hipeers', 'V10166383', 'mercadopulgas', '0', 'Plaza Lara, Carrera 18 entre Calles 22 y 23, Universitas Fundación,', '0', 'Caracas', 'Distrito Capital', 'caracas', '04145289904', '04160000000', '02510000000', 'vladimirve@yahoo.com', '$2y$10$5gFsFR2O.Jdim6Iju0zHgeJbQYo5VElyI34VIVu5Zol7sA8/JqtiO', 'uFIU13CKyZJfBSUL129Uy4KBpcp82XfrhgXYjnurKfONTmmyfi4db1Vb0aas', '2017-05-21 00:35:24', '2017-05-21 00:35:24', NULL);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD CONSTRAINT `articulos_ibfk_1` FOREIGN KEY (`id_cat`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `articulos_ibfk_2` FOREIGN KEY (`id_usu`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD CONSTRAINT `preguntas_ibfk_1` FOREIGN KEY (`id_pre`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `preguntas_ibfk_2` FOREIGN KEY (`id_rep`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `preguntas_ibfk_3` FOREIGN KEY (`id_art`) REFERENCES `articulos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
